<?php
/*
Plugin Name: fivepost-wp
Plugin URI:
Description: 5Post — федеральный логистический сервис, дочернее подразделение X5 Group. Мы осуществляем доставку заказов из интернет-магазинов и маркетплейсов наших партнеров в пункты выдачи заказов и постаматы в магазинах "Пятёрочка" или "Перекрёсток".
Version: 1.0.0
Author URI: https://ipol.ru
Text Domain: fivepost-wp
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

use WordPress\Fivepost\Fivepost_WP;

if(!defined('ABSPATH') || !in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))))
    exit; // Exit if accessed directly

const FIVEPOST_PLUGIN_VERSION = '1.0.0';
const FIVEPOST_PLUGIN_REQUIRES = '5.0';
const FIVEPOST_PLUGIN_TESTED = '5.8.2';
const FIVEPOST_NAMESPACE = 'WordPress\Fivepost';
const FIVEPOST_LIB_NAMESPACE = 'Ipol\Fivepost';
const FIVEPOST_PLUGIN_DOMAIN = 'fivepost-wp';
//const FIVEPOST_PLUGIN_DIR = dirname(__FILE__) . DIRECTORY_SEPARATOR;

define('FIVEPOST_PLUGIN_DIR', plugin_dir_path(__FILE__) );
define('FIVEPOST_PLUGIN_BASENAME', plugin_basename(__FILE__));
define('FIVEPOST_PLUGIN_URI', plugin_dir_url(__FILE__));

require_once(FIVEPOST_PLUGIN_DIR.'autoload.php');

$app = Fivepost_WP::getInstance();
$app->run();