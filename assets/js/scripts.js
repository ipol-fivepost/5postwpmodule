(function($) {
    $(document).ready(function() {
        $(this).on('click', '.button-map', function() {
            $(this).closest('label').trigger('click');
        });

        $(this).on('click', '[data-post5_popup]', function() {
            var id = $(this).data('post5_popup');
            var popup = $('#' + id);
            var payment_method=$('input[name=payment_method]:checked').val();
            var iscard=window.fivepost_systemdlv_card.includes(payment_method),iscash=window.fivepost_systemdlv_cash.includes(payment_method);

            if(popup.length) {
                popup.addClass('open');

                ymaps.ready(function () {
                    var sumLat = 0., sumLong = 0.;
                    var mapContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                    );

                    if(id == 'post5-map-popup') {
                        window.pickupPoints.forEach(function(point) {
                            sumLat += parseFloat(point.latitude);
                            sumLong += parseFloat(point.longitude);
                        });

                        window.pickupMap = new ymaps.Map(popup.data('map'), {
                            center: [sumLat / window.pickupPoints.length, sumLong / window.pickupPoints.length],
                            zoom: 11
                        }, {
                            searchControlProvider: 'yandex#search'
                        });

                        window.pickupPoints.forEach(function(point) {
                            var pointicon=point.iconUrl,contfooter='<button type="button" class="post5-choose-point" data-point=\'' + JSON.stringify({
                                id: point.id,
                                name: point.name,
                                zone: point.zone,
                                pcash:point.pcash,
                                pcard:point.pcard
                            }) + '\'>Выбрать</button>';
                            if (iscash && (point.pcash!='1')) {
                                contfooter='<div class="post5-inactive-point">Оплата выбранной платежной системой на данной точке недоступна. Выберите другой способ оплаты в оформлении заказа, либо другую точку выдачи.</div>';
                                pointicon+='_inactive';
                            }
                            if (iscard && (point.pcard!='1')) {
                                contfooter='<div class="post5-inactive-point">Оплата выбранной платежной системой на данной точке недоступна. Выберите другой способ оплаты в оформлении заказа, либо другую точку выдачи.</div>';
                                pointicon+='_inactive';
                            }
                            pointicon+='.png';
                            pickupMap.geoObjects.add(new ymaps.Placemark([parseFloat(point.latitude), parseFloat(point.longitude)], {
                                hintContent: point.name,
                                balloonContentHeader: point.name,
                                balloonContentBody: point.popupContent,
                                balloonContentFooter: contfooter,
                            }, {
                                iconLayout: 'default#imageWithContent',
                                iconImageHref: pointicon,
                                iconImageSize: [40, 40],
                                iconImageOffset: [-20, -40],
                                iconContentOffset: [0, 0],
                                iconContentLayout: mapContentLayout
                            }));
                        });
                    }
                });
            }
        });

        $(this).on('click', '.post5-popup-close', function() {
            $('.post5-popup.open').removeClass('open');

            if(window.pickupMap !== undefined)
                window.pickupMap.destroy();
        });

        $(this).on('click', '.post5-choose-point', function() {
            var pointData = $(this).data('point');
            /*
            if($('[data-post5_popup="post5-map-popup"]').length) {
                $('#post5-choosen-data').remove();

                $('[data-post5_popup="post5-map-popup"]').before(
                    '<span id="post5-choosen-data">' +
                      '<span><b>' + pointData.name + '</b></span>' +
                    '</span>'
                );
            }*/

            if($('#fivepost_point_id').length)
                $('#fivepost_point_id').val(pointData.id);
            if($('#fivepost_point_zone').length)
                $('#fivepost_point_zone').val(pointData.zone);
            if($('#fivepost_point_pay_cash').length)
                $('#fivepost_point_pay_cash').val(pointData.pcash);
            if($('#fivepost_point_pay_card').length)
                $('#fivepost_point_pay_card').val(pointData.pcard);

            $('.post5-popup.open').removeClass('open');

            if(window.pickupMap !== undefined)
                window.pickupMap.destroy();

            $(document.body).trigger('update_checkout', {
                clear: false
            });
        });
    });
})(jQuery)