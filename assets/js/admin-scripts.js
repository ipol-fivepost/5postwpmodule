(function($) {
    $(document).ready(function() {
        if($('#woocommerce_fivepost_shipping_method_api_key').length && $('#woocommerce_fivepost_shipping_method_api_key').val()) {
            let settingsForm = $('#woocommerce_fivepost_shipping_method_api_key').closest('#mainform');

            $.ajax({
                url: wp_ajax.url,
                type: 'POST',
                data: {
                    action: 'load_warehouses_form'
                },
                dataType: 'json',
                success: function(response) {
                    settingsForm.after(response.html);

                    setTimeout(function() {
                        $('#woocommerce_fivepost_create_warehouse_region_code').inputmask('99');
                        $('#woocommerce_fivepost_create_warehouse_time_zone').inputmask('(+|-)(01|02|03|04|05|06|07|08|09|10|11|12):00');
                        $('#woocommerce_fivepost_create_warehouse_contact_phone_number').inputmask('+79999999999');
                        $('.time-inputmask').inputmask({ alias: "datetime", inputFormat: "HH:MM"});
                    }, 100);
                },
                error: function(err) {
                    settingsForm.after(
                        '<div class="error inline"><p><strong>' + fivepost_localization.error_warehouses_load + '</strong></p></div>'
                    );
                }
            });

            $(this).on('click', '.set-warehouse-default', function() {
                if(confirm(fivepost_localization.error_warehouses_set_default_question)) {
                    let warehouseId = $(this).data('warehouse');

                    $.ajax({
                        url: wp_ajax.url,
                        type: 'POST',
                        data: {
                            action: 'set_warehouse_default',
                            warehouseId: warehouseId
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response.error.length)
                                $('#warehouse-' + warehouseId).prepend(
                                    '<div class="error inline"><p><strong>' + response.error + '</strong></p></div>'
                                );
                            if (response.success.length) {
                                $('#warehouse-' + warehouseId).prepend(
                                    '<div class="updated inline"><p><strong>' + response.success + '</strong></p></div>'
                                );

                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                        },
                        error: function (err) {
                            $('#warehouse-' + warehouseId).prepend(
                                '<div class="error inline"><p><strong>' + fivepost_localization.error_warehouses_set_default + '</strong></p></div>'
                            )
                        }
                    });
                }
            });

            $(this).on('click', '.remove-warehouse', function() {
                if(confirm(fivepost_localization.error_warehouses_remove_question)) {
                    let warehouseId = $(this).data('warehouse');

                    $.ajax({
                        url: wp_ajax.url,
                        type: 'POST',
                        data: {
                            action: 'remove_warehouse',
                            warehouseId: warehouseId
                        },
                        dataType: 'json',
                        success: function (response) {
                            if (response.error.length)
                                $('#warehouse-' + warehouseId).prepend(
                                    '<div class="error inline"><p><strong>' + response.error + '</strong></p></div>'
                                );
                            if (response.success.length) {
                                $('#warehouse-' + warehouseId).prepend(
                                    '<div class="updated inline"><p><strong>' + response.success + '</strong></p></div>'
                                );

                                setTimeout(function () {
                                    window.location.reload();
                                }, 1000);
                            }
                        },
                        error: function (err) {
                            $('#warehouse-' + warehouseId).prepend(
                                '<div class="error inline"><p><strong>' + fivepost_localization.error_warehouses_remove + '</strong></p></div>'
                            )
                        }
                    });
                }
            });

            $(this).on('click', '#show-create-warehouse', function() {
                $('#import-warehouses-wrapper').hide();
                $('#create-warehouse-wrapper').show();
                $('#woocommerce_fivepost_warehouses_action').val('create_warehouse');
            });

            $(this).on('click', '#show-import-warehouses', function() {
                $('#create-warehouse-wrapper').hide();
                $('#import-warehouses-wrapper').show();
                $('#woocommerce_fivepost_warehouses_action').val('import_warehouses');
            });

            $(this).on('click', '#create-warehouse', function() {
                let formData = new FormData($('#warehousesform').get(0));
                $('.create-warehouse-message').remove();

                $.ajax({
                    url: wp_ajax.url,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        $('#warehousesform').append(
                            (response.errors.length ? response.errors : '') +
                            (response.warnings.length ? response.warnings : '') +
                            (response.success.length ? response.success : '')
                        );

                        if(response.success.length) {
                            setTimeout(function() {
                                window.location.reload();
                            }, 1000);
                        }
                    },
                    error: function(err) {
                        $('#warehousesform').append(
                            '<div class="error inline create-warehouse-message"><p><strong>' + fivepost_localization.error_warehouses_create + '</strong></p></div>'
                        );
                    }
                });
            });

            $(this).on('click', '#import-warehouses', function() {
                let formData = new FormData($('#warehousesform').get(0));
                $('.warehouse-message').remove();

                $.ajax({
                    url: wp_ajax.url,
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        settingsForm.next('.error').remove();
                        settingsForm.after(
                            (response.errors.length ? response.errors : '') +
                            (response.warnings.length ? response.warnings : '') +
                            (response.success.length ? response.success : '')
                        );

                        if(response.success.length) {
                            $('#woocommerce_fivepost_import_warehouses').val('');

                            setTimeout(function () {
                                window.location.reload();
                            }, 1000);
                        }
                    },
                    error: function(err) {
                        settingsForm.next('.error').remove();
                        settingsForm.after(
                            '<div class="error inline"><p><strong>' + fivepost_localization.error_warehouses_import + '</strong></p></div>'
                        );
                    }
                });
            });

            $(this).on('click', '#export-warehouses', function() {
                $.ajax({
                    url: wp_ajax.url,
                    type: 'POST',
                    data: {
                        action: 'export_warehouses'
                    },
                    dataType: 'json',
                    success: function(response) {
                        let link = document.createElement('a');
                        document.body.appendChild(link);
                        link.download = 'settings';
                        link.target = '_blank';
                        link.href=response.url;
                        link.click();
                        link.remove();
                    },
                    error: function(err) {}
                });
            });
        }

        $(this).on('click', '#change-shipping-method', function() {
            $('.change-shipping-form').show();
            $(this).hide();
        });

        $(this).on('click', '.button-map', function() {
            $(this).closest('label').trigger('click');
        });

        $(this).on('click', '[data-post5_popup]', function() {
            var id = $(this).data('post5_popup');
            var popup = $('#' + id);

            if(popup.length) {
                popup.addClass('open');

                ymaps.ready(function () {
                    var sumLat = 0., sumLong = 0.;
                    var mapContentLayout = ymaps.templateLayoutFactory.createClass(
                        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
                    );

                    if(id == 'post5-map-popup') {
                        window.pickupPoints.forEach(function(point) {
                            sumLat += parseFloat(point.latitude);
                            sumLong += parseFloat(point.longitude);
                        });

                        window.pickupMap = new ymaps.Map(popup.data('map'), {
                            center: [sumLat / window.pickupPoints.length, sumLong / window.pickupPoints.length],
                            zoom: 11
                        }, {
                            searchControlProvider: 'yandex#search'
                        });

                        window.pickupPoints.forEach(function(point) {
                            pickupMap.geoObjects.add(new ymaps.Placemark([parseFloat(point.latitude), parseFloat(point.longitude)], {
                                hintContent: point.name,
                                balloonContentHeader: point.name,
                                balloonContentBody: point.popupContent,
                                balloonContentFooter: '<button type="button" class="post5-choose-point" data-point=\'' + JSON.stringify({
                                    id: point.id,
                                    name: point.name + ': ' + point.address,
                                    zone: point.zone,
                                    cost: point.cost,
                                    costHtml: point.costHtml
                                }) + '\'>Выбрать</button>',
                            }, {
                                iconLayout: 'default#imageWithContent',
                                iconImageHref: point.iconUrl,
                                iconImageSize: [40, 40],
                                iconImageOffset: [-20, -40],
                                iconContentOffset: [0, 0],
                                iconContentLayout: mapContentLayout
                            }));
                        });
                    }
                });
            }
        });

        $(this).on('click', '.post5-popup-close', function() {
            $('.post5-popup.open').removeClass('open');

            if(window.pickupMap !== undefined)
                window.pickupMap.destroy();
        });

        $(this).on('change', '[name="Order[rate]"]', function() {
            $('#save-shipping-method').data('point', '');
        });

        $(this).on('click', '.post5-choose-point', function() {
            let pointData = $(this).data('point');
            let shippingTitle = $('[name="Order[rate]"]:checked').next().children('.rate-name').text() + ' (' + pointData.name + ')';

            $('.shipping-info-title').text(shippingTitle);
            $('.rate-name .rate-cost').remove();
            $('.rate-name').append(' <span class="rate-cost">(' + pointData.costHtml + ')</span>');
            $('#save-shipping-method').data('point', $(this).data('point'));

            $('.post5-popup.open').removeClass('open');

            if(window.pickupMap !== undefined)
                window.pickupMap.destroy();
        });

        $(this).on('click', '#save-shipping-method', function() {
            let orderID = $(this).data('order');
            let pointData = $(this).data('point');

            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_update_shipping',
                    order_id: orderID,
                    point: pointData
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    if(response.type == 'error')
                        alert(response.message);
                    else
                        window.location.reload();
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $(this).on('change', '[name="Order[type]"]', function() {
            if($('[name="Order[type]"]:checked').val() == 'LegalPerson')
                $('#company-wrapper').show();
            else
                $('#company-wrapper').hide();
        });

        /*$(this).on('change', '[name="Order[full_paid]"]', function() {
            if($('[name="Order[full_paid]"]').prop('checked'))
                $('#paid-amount-wrapper').hide();
            else
                $('#paid-amount-wrapper').show();
        });*/

        $(this).on('click', '#send-order', function() {
            let orderID = $(this).data('order');

            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_send_order',
                    form: {
                        order_id: orderID,
                        first_name: $('[name="Order[first_name]"]').val(),
                        last_name: $('[name="Order[last_name]"]').val(),
                        phone: $('[name="Order[phone]"]').val(),
                        email: $('[name="Order[email]"]').val(),
                        type: $('[name="Order[type]"]:checked').val(),
                        legal_name: $('[name="Order[legal_name]"]').val(),
                        package_weight: $('[name="Order[package_weight]"]').val(),
                        package_length: $('[name="Order[package_length]"]').val(),
                        package_width: $('[name="Order[package_width]"]').val(),
                        package_height: $('[name="Order[package_height]"]').val(),
                        from_place: $('[name="Order[from_place]"]').length ? $('[name="Order[from_place]"]').val() : false,
                        ptype: $('select.five-post-pay-type option:selected').val(),
                        wh: $('select.five-post-warehouse option:selected').val()
                        //full_paid: $('[name="Order[full_paid]"]').prop('checked') ? 1 : 0,
                        // paid_amount: $('[name="Order[paid_amount]"]').val()
                    }
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    if(response.type == 'error')
                        alert(response.message);
                    else {
                        alert(response.message);

                        window.location.reload();
                    }
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $(this).on('click', '#cancel-order', function() {
            let orderID = $(this).data('order');

            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_cancel_order',
                    order_id: orderID
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    if(response.type == 'error')
                        alert(response.message);
                    else {
                        alert(response.message);

                        window.location.reload();
                    }
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $(this).on('click','#get-barcode',function(){
            let orderID = $(this).data('order');
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_print_label',
                    order_id: orderID
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    if(response.type == 'error')
                        alert(response.message);
                    else {
                        //alert(response.message);
                        if ((response.fname!==undefined) && (response.fname!='')) {
                            $('#fivepost-wp-panel .order-package-printlabel-wrap').html('<div class="order-package-printlabel"><a target="_blank" href="'+response.fname+'">Печать наклейки</a></div>');
                            window.open(response.fname, '_blank');
                        }
                        //window.location.reload();
                    }
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $(this).on('click','#get-orderstatus',function(){
            let orderID = $(this).data('order');
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_get_order_status',
                    order_id: orderID
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    console.log(response);
                    if(response.type == 'error')
                        alert(response.message);
                    else {
                        alert(response.message);
                    }
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $(this).on('click','#save-ordercalc',function(){
            let orderID = $(this).data('order');
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_save_order_calcdata',
                    order_id: orderID,
                    WEI:$('#order_package_weight').val(),
                    L:$('#package_length').val(),
                    W:$('#package_width').val(),
                    H:$('#package_height').val()
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    if(response.type == 'error')
                        alert(response.message);
                    else {
                        window.location.reload();
                    }
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $('.select_fivepost_for_neworder').click(function(){
            let orderID = $(this).data('order');
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_selectthisservice',
                    order_id:orderID
                },
                dataType: 'json',
                beforeSend: function (response) {
                    $('#fivepost-wp-panel').addClass('loading');
                },
                complete: function (response) {
                    $('#fivepost-wp-panel').removeClass('loading');
                },
                success: function(response) {
                    if(response.type == 'error')
                        alert(response.message);
                    else {
                        window.location.reload();
                    }
                },
                error: function() {
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        });

        $('.woocommerce_fivepost_shipping_method_acrd').click(function(){
            var t=$(this);
            if (t.hasClass('sh')) t.removeClass('sh').next().slideUp();
            else t.addClass('sh').next().slideDown();
        });

        //PPWin
        $('#force_renew_points').click(function(){
            $('.fivepost_upd_ppwin .fivepost-progressbar').addClass('hid').find('.progress span').css('width','');
            $('.fivepost_upd_ppwin .fivepost-progressbar p span').html('');
            $('.fivepost_upd_ppwinbg,.fivepost_upd_ppwin').fadeIn();
        });

        $('.fivepost_upd_ppwin .close,.fivepost_upd_ppwin .btnclose').click(function(){
            $('.fivepost_upd_ppwinbg,.fivepost_upd_ppwin').fadeOut();
        });

        var pntloadpage=0,pntload_inprogress=false;
        function get_points() {
            pntload_inprogress=true;
            $('.fivepost_upd_ppwin .fivepost-progressbar').removeClass('hid');
            $.ajax({
                type: 'POST',
                url: '/wp-admin/admin-ajax.php',
                data: {
                    action: 'five_post_force_renew_points',
                    pgn: pntloadpage
                },
                dataType: 'json',
                beforeSend: function (response) {

                },
                complete: function (response) {

                },
                success: function(response) {
                    if (response.ans) {
                        var html = 'Завершено страниц: ' + response.pgn + ' из ' + response.tot,cf = Number(response.pgn)/Number(response.tot)*100;
                        pntloadpage++;
                        if (response.isl) {
                            html = 'Обновление точек успешно завершено.';
                            $('.fivepost_upd_ppwin .fivepost-progressbar .progress span').css('width','100%');
                            //clearInterval(pntload);
                            setTimeout(function(){
                                $('.fivepost_upd_ppwin .fivepost-progressbar').addClass('hid');
                                $('.fivepost_upd_ppwin .fivepost-progressbar p span').html('');
                            },10000);
                            pntload_inprogress=false;
                        } else {
                            $('.fivepost_upd_ppwin .fivepost-progressbar .progress span').css('width',cf+'%');
                            setTimeout(function () {
                                get_points();
                            },5000);
                        }
                        $('.fivepost_upd_ppwin .fivepost-progressbar p span').html(html);
                    } else {
                        switch (response.code) {
                            case -1:
                                alert('Модуль не настроен.');
                                break;
                            case -2:
                                alert('Возникла ошибка или ответ пришел пустым.');
                                break;
                            case -3:
                                alert('Не передан номер страницы для загрузки');
                                break;
                            default:
                                alert('При загрузке точек возникла ошибка.');
                        }
                        $('.fivepost_upd_ppwin .fivepost-progressbar').addClass('hid');
                        $('.fivepost_upd_ppwin .fivepost-progressbar p span').html('');
                        pntload_inprogress=false;
                    }
                },
                error: function() {
                    pntload_inprogress=false;
                    alert('Возникла ошибка. Попробуйте, пожалуйста, позже.');
                }
            })
        }

        $('.fivepost_upd_ppwin .btnforcepntupdate').click(function(){
            if (pntload_inprogress) return;
            pntloadpage=0;
            get_points();
        });


        // /PPWin

        $('.five-post-pay-type option:selected').prop('selected',false);
        $('.five-post-pay-type option.s').prop('selected',true);
        $('.five-post-warehouse option:selected').prop('selected',false);
        $('.five-post-warehouse option.s').prop('selected',true);
        setTimeout(function(){
            $('#order_phone').val($('#order_phone').attr('value'));
            $('#order_email').val($('#order_email').attr('value'));
            $('#package_length').val($('#package_length').attr('value'));
            $('#package_width').val($('#package_width').attr('value'));
            $('#package_height').val($('#package_height').attr('value'));
            $('#order_package_weight').val($('#order_package_weight').attr('value'));
        },1000);
    });
})(jQuery)