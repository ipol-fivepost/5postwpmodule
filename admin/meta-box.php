<?php
/**
 * @var array $data
 * @var \WC_Shipping_Rate $rate
*/
?>
<?php if(!empty($data['fivepost']) && !empty($data['fivepost']['order_id'])):?>
    <div class="order-info-wrapper">
        <div class="order-info">
            <?php _e('Order created with number','fivepost-wp')._e(sprintf(' <b>№%s</b>', $data['fivepost']['sender_order_id']), 'fivepost-wp')?>
        </div>
        <div class="order-packages">
            <div class="order-package">
                <?php if(!empty($data['fivepost']['history'])):
                    $main_statuses = Fivepost_Shipping_Method::get_main_statuses();
                    $execution_statuses = Fivepost_Shipping_Method::get_execution_statuses()?>
                    <div class="order-package-history">
                        <div class="order-package-history-title"><?php _e('History', 'fivepost-wp')?></div>
                        <div class="order-package-history-rows">
                            <?php foreach($data['fivepost']['history'] as $row):
                                $date = new \DateTime($row['changeDate'])?>
                                <div class="order-package-history-row">
                                    <div class="history-date"><?php echo esc_html(htmlspecialchars($date->format('d.m.Y H:i:s'))) ?></div>
                                    <div class="history-status">
                                        <?php echo esc_html(htmlspecialchars(
                                            !empty($main_statuses[$row['status']]) ? $main_statuses[$row['status']] :
                                                $row['status']
                                        )); ?> /
                                        <?php echo esc_html(htmlspecialchars(
                                            !empty($execution_statuses[$row['executionStatus']]) ? $execution_statuses[$row['executionStatus']] :
                                                $row['executionStatus']
                                        )); ?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endif;?>
                <div class="order-package-actions">
                    <button type="button" class="button button-primary" id="get-orderstatus" data-order="<?php echo esc_attr(htmlspecialchars($data['id'])) ?>">
                        <?php _e('Get order status', 'fivepost-wp')?>
                    </button>
                </div>
                <div class="order-package-actions" style="margin-top:10px;">
                    <?php if ($data['fivepost']['printlabel']) { ?>
                        <a target="_blank" class="button button-primary" href="<?php echo esc_url(FIVEPOST_PLUGIN_URI.'files/'.$data['fivepost']['printlabel'])?>"><?php _e('Print printlabel', 'fivepost-wp')?></a>
                    <?php } else { ?>
                        <button type="button" class="button button-primary" id="get-barcode" data-order="<?php echo esc_attr(htmlspecialchars($data['id'])) ?>">
                            <?php _e('Print printlabel', 'fivepost-wp')?>
                        </button>
                    <?php } ?>
                    <button type="button" class="button button-primary" id="cancel-order"
                            data-order="<?php echo esc_attr(htmlspecialchars($data['id'])) ?>"
                    >
                        <?php _e('Cancel order', 'fivepost-wp')?>
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php else:?>
    <div class="change-wrapper">
        <?php if(!empty($data['shipping']['title'])):?>
            <div class="shipping-info-wrapper">
                <?php if(!empty($data['shipping']['title'])):?>
                    <div class="shipping-info-title"><?php echo esc_html(htmlspecialchars($data['shipping']['title'])) ?></div>
                <?php endif;?>
                <?php if($data['shipping']['emptypoint']):?>
                    <div class="shipping-info-point"><?php _e('Attention! You need to choose a Delivery Point!','fivepost-wp') ?></div>
                <?php endif;?>
            </div>
        <?php endif?>
        <div class="post5-form change-shipping-form">
            <?php if(!empty($data['shipping']['rates'])):?>
                <div class="form-row">
                    <?php foreach($data['shipping']['rates'] as $rate):
                        $rate_meta = $rate->get_meta_data();
                    ?>
                        <label>
                            <input style="display:none" type="radio" name="Order[rate]" value="<?php echo esc_attr(htmlspecialchars($rate->get_id())) ?>"
                                checked
                                data-name="<?php echo esc_attr(htmlspecialchars($rate->get_label())) ?>"
                                data-cost="<?php echo esc_attr(htmlspecialchars($rate->get_cost())) ?>"
                            />
                            <span><span class="rate-name"><?php echo esc_html($rate->get_label())?></span></span>
                            <?php if ($rate->get_id() === 'fivepost_shipping_method:pickup'):?>
                                <script>
                                    window.pickupPoints = [];
                                    <?php
                                    $meta_data = $rate->get_meta_data();
                                    $days = [
                                        'MON' => __('Monday', 'fivepost-wp'),
                                        'TUE' => __('Tuesday', 'fivepost-wp'),
                                        'WED' => __('Wednesday', 'fivepost-wp'),
                                        'THU' => __('Thursday', 'fivepost-wp'),
                                        'FRI' => __('Friday', 'fivepost-wp'),
                                        'SAT' => __('Saturday', 'fivepost-wp'),
                                        'SUN' => __('Sunday', 'fivepost-wp'),
                                    ];
                                    foreach($meta_data['points'] as $point):
                                        $work_hours = @json_decode($point->work_hours);
                                        $schedule = [];
                                        $payment_methods = [];
                                        $delivery_price = 0;

                                        if(!empty($meta_data['rates'][$point->zone]))
                                            $delivery_price = $meta_data['rates'][$point->zone]->delivery_price;

                                        if(!$delivery_price) continue;

                                        foreach ($work_hours as $item) {
                                            if(isset($days[$item->day])) {
                                                $time_start = new \DateTime($item->opensAt);
                                                $time_end = new \DateTime($item->closesAt);
                                                $schedule[] = '<div class="point-worktime-day">'.
                                                    $days[$item->day].': '.$time_start->format('H:i').' - '.$time_end->format('H:i').
                                                    '</div>';
                                            }
                                        }

                                        if($point->cash_allowed)
                                            $payment_methods[] = __('Cash payment', 'fivepost-wp');

                                        if($point->card_allowed)
                                            $payment_methods[] = __('Card payment', 'fivepost-wp');

                                        if($point->loyalty_allowed)
                                            $payment_methods[] = __('Loyalty payment', 'fivepost-wp');

                                        $dlv_time = intval($point->delivery_sl)+intval($data['shipping']['addDays']);
                                        $popup_content =
                                            '<div class="point-address-wrapper">'.
                                                '<div class="point-address-content">'.$point->full_address.'</div>'.
                                            '</div>'.
                                            '<div class="point-worktime-wrapper" style="margin-top: 10px;">'.
                                                '<div class="point-worktime-title"><strong>'.__('Work time', 'fivepost-wp').':</strong></div>'.
                                                '<div class="point-worktime-content">'.implode('', $schedule).'</div>'.
                                            '</div>'.
                                            '<div class="point-description-wrapper" style="margin-top: 10px">'.
                                                '<div class="point-description-title"><strong>'.__('Description:', 'fivepost-wp').'</strong></div>'.
                                                '<div class="point-description-content">'.$point->additional.'</div>'.
                                            '</div>'.
                                            '<div class="point-payment-wrapper" style="margin-top: 10px;">'.
                                                '<div class="point-payment-title"><strong>'.__('Payment Methods:', 'fivepost-wp').'</strong></div>'.
                                                '<div class="point-payment-content" style="font-weight: bold">'.implode(', ', $payment_methods).'</div>'.
                                            '</div>'.
                                            '<div class="point-total-wrapper" style="margin-top: 10px;"><strong>'.__('Delivery time', 'fivepost-wp').': </strong>'.\Ipol\Fivepost\WordPress\Tools\Tools::declination($dlv_time, [__('day', 'fivepost-wp'), _x('days','2_days', 'fivepost-wp'), _x('days','many_days', 'fivepost-wp')]).'</div>'.
                                            '<div class="point-total-wrapper" style="margin-top: 10px;">'.
                                                '<strong>'.__('Price:', 'fivepost-wp').'</strong> '.wc_price($delivery_price).
                                            '</div>';
                                        $icon_url = FIVEPOST_PLUGIN_URI.'/assets/images/png/'.$point->type.'.png';?>
                                        pickupPoints.push({
                                            id: '<?php echo esc_js(htmlspecialchars($point->id)); ?>',
                                            name: '<?php echo esc_js(htmlspecialchars($point->name)); ?>',
                                            zone: '<?php echo esc_js(htmlspecialchars($point->zone)); ?>',
                                            address: '<?php echo esc_js(htmlspecialchars($point->full_address)); ?>',
                                            latitude: '<?php echo esc_js(htmlspecialchars($point->latitude)); ?>',
                                            longitude: '<?php echo esc_js(htmlspecialchars($point->longitude)); ?>',
                                            iconUrl: '<?php echo esc_js($icon_url); ?>',
                                            popupContent: '<?php echo wp_kses($popup_content,[
                                                    'div'=>[
                                                        'class'=>[],
                                                        'style'=>[]
                                                    ],
                                                'strong'=>[]
                                            ]); ?>',
                                            cost: '<?php echo esc_js(round($delivery_price)); ?>',
                                            costHtml: '<?php echo wp_kses(wc_price($delivery_price),[
                                                'span'=>['class'=>''],
                                                'bdi'=>[]
                                            ]); ?>'
                                        });
                                    <?php endforeach;?>
                                </script>
                                <a href="javascript:void(0);" class="button-map" data-post5_popup="post5-map-popup">
                                    <?php _e('Select on map', 'fivepost-wp')?>
                                </a>
                            <?php endif;?>
                        </label>
                    <?php endforeach;?>
                </div>
                <div class="form-row form-row-submit">
                    <button type="button" class="button button-primary" id="save-shipping-method"
                        data-order="<?php echo esc_attr(htmlspecialchars($data['id'])); ?>">
                        <?php _e('Save', 'fivepost-wp')?>
                    </button>
                </div>
            <?php endif?>
        </div>
        <a href="javascript:void(0);" id="change-shipping-method"><?php _e('Change')?></a>
    </div>
    <div class="create-order-form-wrapper">
        <div class="post5-form create-order-form">
            <div class="form-row">
                <label for="order_name"><?php _e('Buyer First Name', 'fivepost-wp')?></label>
                <input type="text" name="Order[first_name]" id="order_name" value="<?php echo esc_attr(htmlspecialchars($data['first_name'])) ?>" />
            </div>
            <div class="form-row">
                <label for="order_name"><?php _e('Buyer Last Name', 'fivepost-wp')?></label>
                <input type="text" name="Order[last_name]" id="order_name" value="<?php echo esc_attr(htmlspecialchars($data['last_name'])) ?>" />
            </div>
            <div class="form-row">
                <label for="order_phone"><?php _e('Buyer Phone', 'fivepost-wp')?></label>
                <input type="text" name="Order[phone]" id="order_phone" value="<?php echo esc_attr(htmlspecialchars($data['phone'])) ?>" />
            </div>
            <div class="form-row">
                <label for="order_email"><?php _e('Buyer E-mail', 'fivepost-wp')?></label>
                <input type="text" name="Order[email]" id="order_email" value="<?php echo esc_attr(htmlspecialchars($data['email'])) ?>" />
            </div>
            <div class="form-row">
                <label for="order_email"><?php _e('Package Weight (kg)', 'fivepost-wp')?></label>
                <input type="text" name="Order[package_weight]" id="order_package_weight" value="<?php echo esc_attr(htmlspecialchars($data['package']['weight'])) ?>" />
            </div>
            <div class="form-row">
                <label for="package_length"><?php _e('Package Dimensions (cm)', 'fivepost-wp')?></label>
                <div class="form-column-flex">
                    <input type="text" name="Order[package_length]" id="package_length" value="<?php echo esc_attr(htmlspecialchars($data['package']['length'])) ?>" />
                    <span>x</span>
                    <input type="text" name="Order[package_width]" id="package_width" value="<?php echo esc_attr(htmlspecialchars($data['package']['width'])) ?>" />
                    <span>x</span>
                    <input type="text" name="Order[package_height]" id="package_height" value="<?php echo esc_attr(htmlspecialchars($data['package']['height'])) ?>" />
                </div>
                <button type="button" class="button button-primary" id="save-ordercalc"
                        data-order="<?php echo esc_attr(htmlspecialchars($data['id'])) ?>"
                >
                    <?php _e('Save dimensions and weight data', 'fivepost-wp')?>
                </button>
            </div>
            <?php if (count($data['warehouses'])>0) { ?>
            <div class="form-row">
                <label><?php _e('Select warehouse', 'fivepost-wp')?></label>
                <select name="warehouse" class="five-post-warehouse">
                    <?
                        foreach ($data['warehouses'] as $wh) {
                            ?><option value="<?php echo esc_attr($wh['id'])?>"<?php echo esc_attr( ($wh['default']==1)?' class="s"':'' )?>><?php echo esc_attr($wh['name'])?></option><?
                        }
                    ?>
                </select>
            </div>
            <?}?>
            <div class="form-row">
                <label><?php _e('Payment Method', 'fivepost-wp')?></label>
                <select name="ptype" class="five-post-pay-type">
                    <option value="0"<?php echo esc_attr( ($data['pay_method']==0)?' class="s"':'' )?>><?php _e('Prepayment', 'fivepost-wp')?></option>
                    <option value="1"<?php echo esc_attr( ($data['pay_method']==1)?' class="s"':'' )?>><?php _e('Cash payment', 'fivepost-wp')?></option>
                    <option value="2"<?php echo esc_attr( ($data['pay_method']==2)?' class="s"':'' )?>><?php _e('Card payment', 'fivepost-wp')?></option>
                </select>
            </div>
            <div class="form-row form-row-submit">
                <button type="button" class="button button-primary" id="send-order"
                    data-order="<?php echo esc_attr(htmlspecialchars($data['id'])) ?>">
                    <?php _e('Send Order', 'fivepost-wp')?>
                </button>
            </div>
        </div>
    </div>
<?php endif;