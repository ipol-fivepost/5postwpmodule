<div class="form-table">
    <p class="fivepost-orange-box">Перед началом работы необходимо создать склады, с которых вы отгружаете заказы. Все поля являются обязательными.<br/><br/>
Необходимо уведомить 5POST об открытии и добавлении нового склада для создания связки склад партнера-склад 5POST. Без этого доставить заказ невозможно Одновременно можно использовать несколько складов. Список складов хранится в базе данных.<br/><br/>
При необходимости редактировать склад обратитесь в 5POST.<br/><br/>
Выбрать склад можно при создании заявки.<br/><br/>
Если вы используете несколько складов и несколько тарифных планов, рекомендуем установить опцию “Автовыбор минимальной стоимости” во вкладке Калькуляция. Рекомендуем быть внимательными, при заполнении полей, так как отредактировать либо удалить склад самостоятельно невозможно. В этом случае обращайтесь в 5POST. Учтите, что, если вы создали склад на тестовом аккаунте - его надо будет создать заново на боевом. </p>
</div>