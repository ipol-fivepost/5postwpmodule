<tr valign="top">
    <td colspan="2">
        <div class="fivepost_upd_ppwinbg"></div>
        <div class="fivepost_upd_ppwin">
            <div class="close"></div>

            <p class="h">Обновление точек</p>

            <p>Вы можете принудительно обновить точки выдачи.</p>
            <p>Рекомендация: запрашивать точки выдачи 1 раз в сутки в первой половине дня (после 5 утра).</p>
            <p>Не забывайте - обновление точек все еще работает в автоматическом режиме.</p>

            <div class="fivepost-progressbar">
                <p>Прогресс: <span>Завершено страниц: 1 из 10</span></p>
                <div class="progress"><span></span></div>
            </div>

            <div class="inline">
                <button class="button-primary btnforcepntupdate" type="button">Обновить принудительно</button>
                <button class="button-primary btnclose" type="button">Закрыть</button>
            </div>

        </div>
        <button class="button-primary" type="button" id="force_renew_points">Принудительно запустить процедуру обновления точек выдачи</button>
    </td>
</tr>
