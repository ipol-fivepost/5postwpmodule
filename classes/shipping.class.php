<?php

use Ipol\Fivepost\Core\Delivery\Cargo;
use Ipol\Fivepost\Core\Delivery\CargoCollection;
use Ipol\Fivepost\Core\Delivery\CargoItem;
use Ipol\Fivepost\Core\Delivery\Location;
use Ipol\Fivepost\Core\Delivery\Shipment;
use Ipol\Fivepost\Core\Entity\Money;

class Fivepost_Shipping_Method extends \WC_Shipping_Method {

    const SHIPPING_METHOD_ID = 'fivepost_shipping_method';
    private $statichtmltypes=[];

    /**
     * Constructor for shipping class
     *
     * @access public
     * @return void
     */
    public function __construct() {
        $this->id           = self::SHIPPING_METHOD_ID;
        $this->method_title = __('5Post Delivery', 'fivepost-wp');
        $this->title        = __('5Post Delivery', 'fivepost-wp');
        $this->enabled      = $this->get_option('enabled', false);

        add_action('woocommerce_update_options_shipping_'.$this->id, array($this, 'process_admin_options'));

        $this->init();
    }

    /**
     * Init settings
     *
     * @access public
     * @return void
     */
    public function init() {
        $this->init_form_fields();
        $this->init_settings();

        add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
        if (isset($this->settings['delivery_name'])) {
            if ($this->settings['delivery_name']!='') {
                $this->title=$this->settings['delivery_name'];
                $this->method_title=$this->settings['delivery_name'];
            }
        }
    }

    /**
     * @param $form_fields
     * @param $echo
     * @return string|void
     *
     * Custom function for generate fields in WP Setting Page - added custom types "template_"
     */
    public function generate_settings_html( $form_fields = array(), $echo = true ) {
        if ( empty( $form_fields ) ) {
            $form_fields = $this->get_form_fields();
        }
        $html = '';
        foreach ( $form_fields as $k => $v ) {
            $type = $this->get_field_type( $v );
            if (substr($type,0,9)=='template_') {
                $html.=\Ipol\Fivepost\WordPress\Tools\Tools::render(substr($type,9));
                continue;
            }
            if ( method_exists( $this, 'generate_' . $type . '_html' ) ) {
                $html .= $this->{'generate_' . $type . '_html'}( $k, $v );
            } else {
                $html .= $this->generate_text_html( $k, $v );
            }
        }
        return $html;
    }
    /**
     * Init ADMIN settings
    */
    public function init_form_fields() {
        global $wpdb;
        $tarifs_rows = $wpdb->get_results("SELECT * FROM `{$wpdb->base_prefix}fivepost_tarifs` ORDER BY `tarif_id`;");
        $tarifs=[];
        foreach ($tarifs_rows as $t) {
            $tname=$t->tname;
            if ($tname=='WP_Auto_Tarif') $tname=__('Autoselect (minimum cost)', 'fivepost-wp');
            $tarifs[(string)$t->tarif_id]=$tname;
        }

        $this->form_fields = array(
            'html_faq' => [
                'type'  => 'template_faq'
            ],
            'common_title' => [
                'title' => __('General', 'fivepost-wp'),
                'type' => 'title',
            ],
            'enabled' => [
                'title'   => __('Enable/Disable', 'fivepost-wp'),
                'type'    => 'checkbox',
                'default' => 'yes'
            ],
            'delivery_name' => [
                'title'       => __('Delivery service name', 'fivepost-wp'),
                'type'        => 'text',
                'default'=> $this->title,
            ],
            'api_wtype' => [
                'title'   => __('API Requests in', 'fivepost-wp'),
                'type'    => 'select',
                'default' => 'TEST',
                'options'=> [
                    'TEST' => __('TEST mode', 'fivepost-wp'),
                    'API' => __('WORK mode', 'fivepost-wp')
                ]
            ],
            'sender_brand' => [
                'title'       => __('Sender brand', 'fivepost-wp'),
                'type'        => 'text',
                'default'=>get_bloginfo('name'),
                'description' => __('It is transmitted to the client in SMS, as the sender of the order. If the field is not transmitted, then the name specified during registration in the 5post system is used for notification', 'fivepost-wp'),
                'desc_tip'    => true
            ],
            'unclaimed_orders' => [
                'title'   => __('How unclaimed orders are handled', 'fivepost-wp'),
                'type'    => 'select',
                'default' => 'mode1',
                'options'=> [
                    'mode1' => __('Return to partner\'s warehouse', 'fivepost-wp'),
                    'mode2' => __('Utilization', 'fivepost-wp')
                ]
            ],
            'always_show_winbox' => [
                'title'   => __('Show order button in orders', 'fivepost-wp'),
                'type'    => 'select',
                'default' => 'always',
                'description' => __('Specifies when to add the button for placing a request for 5Post delivery in the administrative part: always or only if the order is placed with the choice of 5Post delivery. This is true if you use several delivery services on the site.', 'fivepost-wp'),
                'desc_tip'    => true,
                'options'=> [
                    'always' => __('Always', 'fivepost-wp'),
                    'isselected' => __('Module Delivery', 'fivepost-wp')
                ]
            ],
            'enshp_force' => [
                'title'   => __('Do not allow to place an order without the selected pickup point', 'fivepost-wp'),
                'type'    => 'checkbox',
                'default' => 'yes',
                'description'=> __('The module will not allow you to place an order if the pickup point is not selected','fivepost-wp'),
                'desc_tip'=>true
            ],
            'html_getpointsbtn' => [
                'type'  => 'template_forse_get_points_button'
            ],
            'api_title' => [
                'title' => __('Authorization keys', 'fivepost-wp'),
                'type' => 'title'
            ],
            'api_key' => [
                'title'       => 'API Key',//__('API Key', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('The key is issued to access the 5Post service. To get the key, you need to contact 5Post. If there are problems with the key, you should also contact 5Post', 'fivepost-wp'),
                'desc_tip'    => true
            ],
            'partner_number' => [
                'title'       => __('Partner Number', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Unique partner number in the 5Post system. Required to generate a track number. Requested from the 5Post manager', 'fivepost-wp'),
                'desc_tip'    => true
            ],
            'yandex_maps_key' => [
                'title'       => __('Yandex Maps Key', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Yandex.Maps scripts require an API key to work. Get a free API key in the developer s office (choose Javascript API and Geocoder).', 'fivepost-wp'),
                'desc_tip'    => true
            ],
            'dimensions_title' => [
                'title' => __('Default dimensions', 'fivepost-wp'),
                'type' => 'title'
            ],
            'html_calc_title' => [
                'type'  => 'template_calctitle'
            ],
            'default_item_length' => [
                'title'       => __('Default item length (cm)', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Default value item length', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => 10
            ],
            'default_item_width' => [
                'title'       => __('Default item width (cm)', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Default value item width', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => 10
            ],
            'default_item_height' => [
                'title'       => __('Default item height (cm)', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Default value item height', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => 10
            ],
            'default_item_weight' => [
                'title'       => __('Default item weight (kg)', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Default value item weight', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => 1
            ],
            'calc_title' => [
                'title' => __('Tarif settings', 'fivepost-wp'),
                'type' => 'title'
            ],
            'base_rate' => [
                'title'       => __('Base rate (kg)', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('The maximum weight of the order, at which the calculation is based on the base rate. The data must be taken from the contract with 5post', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => ''
            ],
            'overweight_step' => [
                'title'       => __('Overweight step, kg', 'fivepost-wp'),
                'type'        => 'text',
                'description' => __('Overweight step, for which there is a surcharge to the cost of delivery. The data must be taken from the contract with 5post', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => ''
            ],
            'add_dlvtime' => [
                'title'       => __('Increase delivery time', 'fivepost-wp'),
                'type'        => 'number',
                'description' => __('Increases the delivery time by the specified number of days. It is used to record the deadline for picking and sending an order to the 5Post warehouse', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => '0'
            ],
            'inc_dlvprice' => [
                'title'       => __('Markup', 'fivepost-wp').':',
                'type'        => 'number',
                'description' => __('You can set a markup on the delivery cost by selecting a fixed amount in rubles or a percentage of the delivery cost in the markup type', 'fivepost-wp'),
                'desc_tip'    => true,
                'default'     => '0'
            ],
            'inc_dlvprice_type' => [
                'title'   => __('Markup type', 'fivepost-wp').':',
                'type'    => 'select',
                'default' => '0',
                'options'=> [
                    '0' => __('fixed', 'fivepost-wp'),
                    '1' => __('percent', 'fivepost-wp').' (%)'
                ]
            ],
            'tarif_type' => [
                'title'   => __('The type of tariff plan that will be used to calculate the cost of delivery', 'fivepost-wp').':',
                'type'    => 'select',
                'default' => '1',
                'options'=> $tarifs

            ],
        );

        $gateways    = [];
        $wc_gateways = array_filter(WC()->payment_gateways()->get_available_payment_gateways(), function($gateway) {
            return $gateway->enabled == 'yes';
        });

        if ($wc_gateways) {
            foreach($wc_gateways as $gateway) {
                $gateways[ $gateway->id ] = $gateway->title;
            }
        
            $this->form_fields['payment_title'] = [
                'title' => __('Payment systems compliance settings', 'fivepost-wp'),
                'type' => 'title',
            ];

            $this->form_fields['html_payment_title']=[
                'type'=>'template_payments_promt'
            ];

            $this->form_fields['online_payments'] = [
                'title' => __('Card payment upon receipt of the order', 'fivepost-wp'),
                'type' => 'multiselect',
                'options' => $gateways,
            ];

            $this->form_fields['npp_payments'] = [
                'title' => __('Cash payment upon receipt of the order', 'fivepost-wp'),
                'type' => 'multiselect',
                'options' => $gateways,
            ];
        }

        if($wc_statuses =  wc_get_order_statuses()) {
            $this->form_fields['status_title'] = [
                'title' => __('Statuses', 'fivepost-wp'),
                'type' => 'title',
            ];

            $this->form_fields['html_status_title']=[
                'type'=>'template_statuses_promt'
            ];
            
            $options = array_merge(['not_set' => __('N/A', 'fivepost-wp')], $wc_statuses);

            foreach (self::get_gen_statuses() as $code => $status)
                $this->form_fields['fivepost_status_' . $code] = [
                    'title' => sprintf(__('Synchronization for status "%s"', 'fivepost-wp'), $status),
                    'type' => 'select',
                    'description' => sprintf(__('Synchronization for status "%s"', 'fivepost-wp'), $status),
                    'desc_tip' => true,
                    'default' => '',
                    'options' => $options
               ];
        }
    }

    /**
     * Settings form validation
     *
     * @throws Exception
     */
    public function validate_text_field($key, $value) {
        switch($key) {
            case 'api_key':
                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter API key.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'partner_number':
                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter partner number.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                } elseif(!is_numeric($value)) {
                    WC_Admin_Settings::add_error(__('Partner number must be an integer.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'yandex_maps_key':
                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter Yandex Maps key.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'default_item_length':
                $value = strtr((float)$value, [',' => '.']);

                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter default length.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'default_item_width':
                $value = strtr((float)$value, [',' => '.']);

                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter default width.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'default_item_height':
                $value = strtr((float)$value, [',' => '.']);

                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter default height.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'default_item_weight':
                $value = strtr((float)$value, [',' => '.']);

                if(empty($value)) {
                    WC_Admin_Settings::add_error(__('Please, enter default weight.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'base_rate':
                $value = strtr((float)$value, [',' => '.']);

                if(!empty($value) && !is_numeric($value)) {
                    WC_Admin_Settings::add_error(__('Base rate must be a numeric value.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
            case 'overweight_step':
                $value = strtr((float)$value, [',' => '.']);

                if(!empty($value) && !is_numeric($value)) {
                    WC_Admin_Settings::add_error(__('Overweight step must be a numeric value.', 'fivepost-wp'));
                    $this->update_option('enabled', 'no');
                }
                break;
        }

        return $value;
    }

    /**
     * Shipping Calculation Method
     *
     * @access public
     * @param mixed $package
     * @return void
     * @throws Exception
     */
    public function calculate_shipping($package = array()) {
        if(!empty($this->settings['api_key']) && !empty($package) && (!empty($package['destination'])) &&
            (!empty($package['destination']['city']) || !empty($package['destination']['postcode']))) {
            global $wpdb;

            parse_str(!empty($_POST['post_data']) ? sanitize_text_field($_POST['post_data']) : '', $post_data);
            $location_parts = [];

            if (!empty($package['destination']['country'])) {
                if (!empty($countries[$package['destination']['country']]))
                    $location_parts[] = $countries[$package['destination']['country']];
            }

            if (!empty($package['destination']['city']))
                $location_parts[] = $package['destination']['city'];

            $shipment = new Shipment();
            $location = new Location('cms');

            $location->setName(implode(', ', $location_parts));

            if(!empty($package['destination']['state']))
                $location->setRegion($package['destination']['state']);
            if(!empty($package['destination']['postcode']))
                $location->setZip($package['destination']['postcode']);

            $shipment->setTo($location);
            $shipment->setTariff(['Courier', 'PickPoint', 'Postamat']);

            $cargoList = new CargoCollection();
            $cargoBox = new Cargo();

            if (!isset($package['forse'])) {
                foreach ($package['contents'] as $item) {
                    /**
                     * @var WC_Product_Simple $product
                     */
                    $product = $item['data'];

                    for ($i = 1; $i <= $item['quantity']; $i++) {
                        $cargoItemWeight = wc_get_weight($product->get_weight(), 'g');
                        $cargoItemLength = $product->get_length();
                        $cargoItemWidth = $product->get_width();
                        $cargoItemHeight = $product->get_height();

                        if(empty($cargoItemWeight))
                            $cargoItemWeight = (float) wc_get_weight($this->get_option('default_item_weight'), 'g', 'kg');
                        if(empty($cargoItemLength))
                            $cargoItemLength = (float)$this->get_option('default_item_length');
                        if(empty($cargoItemWidth))
                            $cargoItemWidth = (float)$this->get_option('default_item_width');
                        if(empty($cargoItemHeight))
                            $cargoItemHeight = (float)$this->get_option('default_item_height');

                        $cargoItem = new CargoItem();
                        $cargoItem->setPrice(new Money($product->get_price()))
                            ->setWeight($cargoItemWeight)
                            ->setLength($cargoItemLength)
                            ->setWidth($cargoItemWidth)
                            ->setHeight($cargoItemHeight);
                        $cargoBox->add($cargoItem);
                    }
                }
            } else {
                $amount=0;
                foreach ($package['contents'] as $item) {
                    /**
                     * @var WC_Product_Simple $product
                     */
                    $product = $item['data'];
                    for ($i = 1; $i <= $item['quantity']; $i++) $amount+=$product->get_price();
                }
                $cargoItem = new CargoItem();
                $cargoItem->setPrice(new Money($amount))
                    ->setWeight($package['forse']['WEI'])
                    ->setLength($package['forse']['L'])
                    ->setWidth($package['forse']['W'])
                    ->setHeight($package['forse']['H']);
                $cargoBox->add($cargoItem);
            }

            $cargoList->add($cargoBox);
            $shipment->setCargoes($cargoList);

            $points_filter = $this->_prepare_points_filter($shipment, $package['destination']);

            if($points = $this->_get_possible_points($points_filter, $package['destination'])) {
                if ($tarif = $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}fivepost_tarifs` WHERE `tarif_id` = %d;",$this->settings['tarif_type']))) {
                    if ($rates = $this->_get_possible_rates($points, $tarif->tname)) {

                        $cost = 0;

                        foreach ($rates as $rate) {
                            $rate->delivery_price = 0;

                            // Checks if rate values are useful. Otherwise client must ask 5Post manager about delivery rates
                            if (!($rate->rateValue > 0 && $rate->rateExtraValue > 0))
                                continue;

                            // Set delivery price without using VAT rate by default
                            $rate->delivery_price = $this->_calculate_by_weight($shipment, $rate->rateValue, $rate->rateExtraValue);

                            // Deal with VAT rate if given
                            if (!empty($rate->vat)) {
                                // Checks we have rate values with VAT loaded
                                if (($rate->vat > 0) && ($rate->rateValueWithVat > 0) && ($rate->rateExtraValueWithVat > 0))
                                    $rate->delivery_price = $this->_calculate_by_weight($shipment, $rate->rateValueWithVat, $rate->rateExtraValueWithVat);
                            }
                        }

                        $rate_title = $this->get_title();

                        if (!empty($post_data['fivepost_point_id'])) {
                            foreach ($points as $point) {
                                if ($point->id == $post_data['fivepost_point_id']) {
                                    $dlv_time = intval($point->delivery_sl)+intval($this->settings['add_dlvtime']);
                                    $rate_title = $this->get_title().' ('. $point->name . ': ' . $point->full_address.'; '.__('Delivery time','fivepost-wp').': '.\Ipol\Fivepost\WordPress\Tools\Tools::declination($dlv_time, [__('day', 'fivepost-wp'), _x('days','2_days', 'fivepost-wp'), _x('days','many_days', 'fivepost-wp')]).' )';
                                    break;
                                }
                            }
                        }

                        if (!empty($post_data['fivepost_point_zone']) && !empty($rates[$post_data['fivepost_point_zone']]))
                            $cost = $rates[$post_data['fivepost_point_zone']]->delivery_price;

                        $meta_data = new stdClass();
                        $meta_data->points = $points;
                        $meta_data->rates = $rates;

                        $this->add_rate(array(
                            'id' => $this->id . ':pickup',
                            'label' => $rate_title,
                            'cost' => $cost,
                            'meta_data' => $meta_data
                        ));

                    }
                }
            } else {
                if ($this->settings['enshp_force']!='yes')
                $this->add_rate(array(
                    'id' => $this->id . ':pickup',
                    'label' => $this->get_title(),
                    'cost' => 0,
                    'meta_data' => null
                ));
            }
        }
    }

    /**
     * Prepare filter for points search using Shipment data
     *
     * @param Shipment $shipment
     * @return array filter for ORM table
     */
    private function _prepare_points_filter($shipment, $destination) {
        $filter = [
            //'date_sync >= \''.(new \DateTime())->modify('-1 day')->format('Y-m-d').'\''
        ];

        // Deal with Cargos
        $totalWeight = $shipment->getCargoes()->getTotalWeight();
        $cargoDimensions = $shipment->getCargoes()->getTotalDimensions();

        if($totalWeight)
            $filter[] = '(max_weight >= '.($totalWeight * 1000).')';

        if(!empty($cargoDimensions['W']))
            $filter[] = '(max_cell_width >= '.($cargoDimensions['W']*10).')';
        if(!empty($cargoDimensions['L']))
            $filter[] = '(max_cell_length >= '.($cargoDimensions['L']*10).')';
        if(!empty($cargoDimensions['H']))
            $filter[] = '(max_cell_height >= '.($cargoDimensions['H']*10).')';

        if(!empty($destination['postcode']) && !empty($destination['city']))
            $filter[] = '((address_zip_code = '.intval($destination['postcode']).') OR (address_city LIKE \''.addslashes($destination['city']).'%\'))';
        elseif(!empty($destination['postcode']))
            $filter[] = '(address_zip_code = '.intval($destination['postcode']).')';
        elseif(!empty($destination['city']))
            $filter[] = '(address_city LIKE \''.addslashes($destination['city']).'%\')';

        return $filter;
    }

    /**
     * Get possible points for current filter
     *
     * @param array $filter @see Ipol\Fivepost\Bitrix\Controller\TableCalculator::preparePointFilter()
     * @return array
     */
    private function _get_possible_points($filter, $destination) {
        global $wpdb;

        $points = $wpdb->get_results(
            "SELECT * FROM `{$wpdb->base_prefix}fivepost_pickup_points` WHERE ".implode(' AND ', $filter)
        );

        if(!empty($destination['city']) && !empty($points)) {
            $city_lc = mb_strtolower($destination['city']);

            foreach($points as $key => $point) {
                $point_city = mb_strtolower(strtr($point->address_city, [
                    ' '.$point->address_city_type => ''
                ]));

                if($point_city != $city_lc)
                    unset($points[$key]);
            }
        }

        return $points;
    }

    /**
     * Get possible rates for current filter
     *
     * @param array $points
     * @param string $rate_type
     *
     * @return array
     */
    private function _get_possible_rates(&$points, $rate_type) {
        $rates = [];
        foreach($points as $point) {
            $point_rates = @json_decode($point->rates);
            if ($rate_type=='WP_Auto_Tarif') {
                $minrate=false;
                foreach($point_rates as $point_rate) {
                    if (!$minrate) $minrate=$point_rate;
                    if ($minrate->rateValue > $point_rate->rateValue) $minrate=$point_rate;
                    if (!empty($point_rate->vat)) {
                        // Checks we have rate values with VAT loaded
                        if (($point_rate->vat > 0) && ($point_rate->rateValueWithVat > 0) && ($point_rate->rateExtraValueWithVat > 0)) {
                            if ($minrate->rateValueWithVat > $point_rate->rateValueWithVat) $minrate=$point_rate;
                        }
                    }
                }
                if ($minrate) {
                    $point->zone=$minrate->zone;
                    if(!isset($rates[$minrate->zone])) {
                        $rates[$minrate->zone] = $minrate;
                        $rates[$minrate->zone]->period_min = $point->delivery_sl;
                        $rates[$minrate->zone]->period_max = $point->delivery_sl;
                    }
                    $rates[$minrate->zone]->period_min = min($rates[$minrate->zone]->period_min, $point->delivery_sl);
                    $rates[$minrate->zone]->period_max = max($rates[$minrate->zone]->period_max, $point->delivery_sl);
                }
            } else {
                foreach($point_rates as $point_rate) {
                    if($point_rate->rateType != $rate_type) continue;
                    $point->zone = $point_rate->zone;
                    if(!isset($rates[$point_rate->zone])) {
                        $rates[$point_rate->zone] = $point_rate;
                        $rates[$point_rate->zone]->period_min = $point->delivery_sl;
                        $rates[$point_rate->zone]->period_max = $point->delivery_sl;
                    }
                    $rates[$point_rate->zone]->period_min = min($rates[$point_rate->zone]->period_min, $point->delivery_sl);
                    $rates[$point_rate->zone]->period_max = max($rates[$point_rate->zone]->period_max, $point->delivery_sl);
                }
            }
        }
        return $rates;
    }

    /**
     * Calculate delivery price based on shipment weight
     *
     * @param float $rate_value base rate value
     * @param float $rate_extra_value overweight rate value
     * @return float
     */
    private function _calculate_by_weight($shipment, $rate_value, $rate_extra_value) {
        $total_weight = $shipment->getCargoes()->getTotalWeight();
        $total_weight = ceil($total_weight / 1000); // To kg

        // From module options
        $base  = floatval($this->settings['base_rate']);
        $extra = floatval($this->settings['overweight_step']);
        $extra_weight = $total_weight - $base;

        if (($extra_weight > 0) && ($extra > 0))
            $delivery_price = $rate_value + ceil($extra_weight / $extra) * $rate_extra_value;
        else
            $delivery_price = $rate_value;

        //Increase DeliveryPrice
        $incval = floatval($this->settings['inc_dlvprice']);
        $inc_type = intval($this->settings['inc_dlvprice_type']);
        if ($inc_type==1) {
            $delivery_price += ($delivery_price*$incval / 100);
        } else {
            $delivery_price+=$incval;
        }

        return $delivery_price;
    }

    public function map_statuses($status='',$execstatus='') {
        $fp_status = '';
        switch ($status) {
            case 'APPROVED':
                $fp_status='VALID';
                break;
            case 'REJECTED':
                $fp_status = 'REJECTED';
                break;
            case 'IN_PROCESS':
                $fp_status=($execstatus=='PLACED_IN_POSTAMAT')?'INPOSTAMAT':'WAREHOUSE';
                break;
            case 'INTERRUPTED':
                if ($execstatus=='PROBLEM') $fp_status='INTERRUPTED';
                if ($execstatus=='LOST') $fp_status='LOST';
                break;
            case 'UNCLAIMED':
                $fp_status='UNCLAIMED';
                if ( ($execstatus=='READY_FOR_WITHDRAW_FROM_PICKUP_POINT')||($execstatus=='PLACED_IN_POSTAMAT') ) $fp_status='RECLAIM';
                if ($execstatus=='WAITING_FOR_REPICKUP')$fp_status='REPICKUP';
                break;
            case 'CANCELLED':
                $fp_status='CANCELLED';
                break;
            case 'DONE':
                $fp_status='DONE';
                break;
            default:
                $fp_status='Not_Set';
        }
        $new_status = 'not_set';
        if ($fp_status!='Not_Set') $new_status = $this->settings['fivepost_status_'.$fp_status];
        if ( ($new_status!='not_set') || ($new_status!='') ) return $new_status;
        return false;
    }

    public static function get_gen_statuses() {
        return [
            'VALID'=>_x('Application approved','cs_stat', 'fivepost-wp'),
            'REJECTED'=>_x('Order processing error (rejected)','cs_stat', 'fivepost-wp'),
            'WAREHOUSE'=>_x('Order in stock 5Post','cs_stat', 'fivepost-wp'),
            'INPOSTAMAT'=>_x('Order in the cell postamat','cs_stat', 'fivepost-wp'),
            'INTERRUPTED'=>_x('Order processing error (execution aborted)','cs_stat', 'fivepost-wp'),
            'LOST'=>_x('Parcel lost','cs_stat', 'fivepost-wp'),
            'RECLAIM'=>_x('Order in the cell, the term has expired, the receipt is possible','cs_stat', 'fivepost-wp'),
            'REPICKUP'=>_x('Order is ready to be reissued','cs_stat', 'fivepost-wp'),
            'UNCLAIMED'=>_x('The order was not requested','cs_stat', 'fivepost-wp'),
            'CANCELLED'=>_x('Order cancelled','cs_stat', 'fivepost-wp'),
            'DONE'=>_x('Order received by customer','cs_stat', 'fivepost-wp')
        ];
    }

    public static function get_main_statuses() {
        return [
            'NEW' => __('New', 'fivepost-wp'),
            'APPROVED' => __('Approved', 'fivepost-wp'),
            'REJECTED' => __('Rejected', 'fivepost-wp'),
            'IN_PROCESS' => __('In progress', 'fivepost-wp'),
            'DONE' => __('Done', 'fivepost-wp'),
            'INTERRUPTED' => __('Interrupted', 'fivepost-wp'),
            'CANCELLED' => __('Cancelled', 'fivepost-wp'),
            'UNCLAIMED' => __('Unclaimed', 'fivepost-wp')
        ];
    }

    public static function get_execution_statuses() {
        return [
            'CREATED' => __('Created', 'fivepost-wp'),
            'APPROVED' => __('Confirmed', 'fivepost-wp'),
            'REJECTED' => __('Rejected', 'fivepost-wp'),
            'PROBLEM' => __('Problem', 'fivepost-wp'),
            'RECEIVED_IN_WAREHOUSE_BY_PLACES' => __('Accepted at the warehouse according to cargo spaces', 'fivepost-wp'),
            'PRESORTED' => __('Pre-sorting', 'fivepost-wp'),
            'RECEIVED_IN_WAREHOUSE_IN_DETAILS' => __('Accepted in stock in detail', 'fivepost-wp'),
            'SORTED_IN_WAREHOUSE' => __('Sorting in warehouse', 'fivepost-wp'),
            'PLACED_IN_CONSOLIDATION_CELL_IN_WAREHOUSE' => __('Placed in a consolidation cell', 'fivepost-wp'),
            'COMPLECTED_IN_WAREHOUSE' => __('Completed in stock', 'fivepost-wp'),
            'READY_TO_BE_SHIPPED_FROM_WAREHOUSE' => __('Ready to ship from stock', 'fivepost-wp'),
            'SHIPPED' => __('Shipped', 'fivepost-wp'),
            'PLACED_IN_POSTAMAT' => __('Placed in post office', 'fivepost-wp'),
            'PICKED_UP' => __('Issued', 'fivepost-wp'),
            'READY_FOR_WITHDRAW_FROM_PICKUP_POINT' => __('Ready for pickup', 'fivepost-wp'),
            'WITHDRAWN_FROM_PICKUP_POINT' => __('Withdrawn from the point of issue', 'fivepost-wp'),
            'WAITING_FOR_REPICKUP' => __('Awaiting reissuance', 'fivepost-wp'),
            'READY_FOR_RETURN' => __('Ready for return', 'fivepost-wp'),
            'LOST' => __('Lost', 'fivepost-wp'),
            'READY_FOR_UTILIZE' => __('Ready for disposal', 'fivepost-wp'),
            'UTILIZED' => __('Recycled', 'fivepost-wp'),
            'CANCELLED' => __('Canceled', 'fivepost-wp'),
            'RETURNED_TO_PARTNER' => __('Shipped to partner', 'fivepost-wp')
        ];
    }
}