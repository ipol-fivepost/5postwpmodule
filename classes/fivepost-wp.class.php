<?php

namespace WordPress\Fivepost;

use Ipol\Fivepost\Api\Entity\Request\CancelOrderById;
use Ipol\Fivepost\Api\Entity\Request\CreateOrder;
use Ipol\Fivepost\Api\Entity\Request\GetOrderHistory;
use Ipol\Fivepost\Api\Entity\Request\GetOrderLabels;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\Barcode;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\BarcodeList;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\Cargo;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\CargoList;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\Cost;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\PartnerOrder;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\PartnerOrderList;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\ProductValue;
use Ipol\Fivepost\Api\Entity\Request\Part\CreateOrder\ProductValueList;
use Ipol\Fivepost\Api\Entity\Request\Part\Warehouse\WarehouseElem;
use Ipol\Fivepost\Api\Entity\Request\Part\Warehouse\WarehouseElemList;
use Ipol\Fivepost\Api\Entity\Request\Part\Warehouse\WorkingTime;
use Ipol\Fivepost\Api\Entity\Request\Part\Warehouse\WorkingTimeList;
use Ipol\Fivepost\Api\Entity\Request\Warehouse;
use Ipol\Fivepost\Api\Entity\Request\PickupPoints;
use Ipol\Fivepost\Api\Sdk;
use Ipol\Fivepost\Api\Adapter\CurlAdapter;
use Ipol\Fivepost\Api\Entity\Request\JwtGenerate;
use Ipol\Fivepost\Core\Delivery\CargoCollection;
use Ipol\Fivepost\Core\Delivery\CargoItem;
use Ipol\Fivepost\Core\Entity\Money;
use Ipol\Fivepost\Core\Order\Address;
use Ipol\Fivepost\Core\Order\Goods;
use Ipol\Fivepost\Core\Order\Item;
use Ipol\Fivepost\Core\Order\ItemCollection;
use Ipol\Fivepost\Core\Order\Order;
use Ipol\Fivepost\Core\Order\Payment;
use Ipol\Fivepost\Core\Order\Receiver;
use Ipol\Fivepost\Core\Order\ReceiverCollection;
use Ipol\Fivepost\Core\Order\Sender;

//use MailPoet\WP\DateTime;
use Ipol\Fivepost\WordPress\Controller\MainController;

class Fivepost_WP {

    private static $instance;

    public static function getInstance() {
        if(!self::$instance)
            self::$instance = new self();

        return self::$instance;
    }

    /**
     * Activating and deactivating the plugin, registering all filters and actions for the plugin to work
     */
    public function run() {
        
        register_activation_hook(FIVEPOST_PLUGIN_DIR.'/fivepost-wp.php', [$this, 'registerActivationHook']);
        register_deactivation_hook(FIVEPOST_PLUGIN_DIR.'/fivepost-wp.php', [$this, 'registerDeactivationHook']);

        add_filter('plugin_action_links_'.FIVEPOST_PLUGIN_BASENAME, [$this, 'filterPluginActionLinks']);

        add_filter('plugins_api', [$this, 'filterPluginsApi'], 10, 3);
        add_filter('cron_schedules', [$this, 'filterCronSchedules']);
        add_filter('woocommerce_shipping_methods', [$this, 'filterShippingMethods']);
        
        add_filter('woocommerce_checkout_fields', [$this, 'filterWoocommerceCheckoutFields'], 1000);
        add_filter('woocommerce_cart_shipping_method_full_label', [$this, 'actionCartShippingMethodFullLabel'], 10, 2);
        
        add_action('init', [$this, 'actionInit']);
        add_action('wp', [$this, 'actionWP']);
        add_action('five_post_every_minute_event', [$this, 'actionEveryMinuteEvent']);
        add_action('five_post_hourly_event', [$this, 'actionHourlyEvent']);
        add_action('five_post_deleteoldfiles_event', [$this, 'actionDeleteoldfilesEvent']);
        add_action('admin_footer', [$this, 'actionAdminFooter']);
        add_action('wp_footer', [$this, 'actionWPFooter']);
        add_action('woocommerce_shipping_init', [$this, 'actionCourierShippingInit']);
        add_action('woocommerce_after_shipping_rate', [$this, 'actionAfterShippingRate'], 10, 2);
        add_action('woocommerce_checkout_update_order_review', [$this, 'actionWoocommerceCheckoutUpdateOrderReview'], 10);
        add_action('admin_enqueue_scripts', [$this, 'actionAdminEnqueueScripts']);
        add_action('wp_enqueue_scripts', [$this, 'actionWpEnqueueScripts']);
        
        add_action('woocommerce_checkout_create_order', [$this, 'actionWoocommerceCheckoutCreateOrder'], 10, 2);
        add_action('wp_ajax_load_warehouses_form', [$this, 'actionWpAjaxLoadWarehousesForm'], 10, 2);
        add_action('wp_ajax_set_warehouse_default', [$this, 'actionWpAjaxSetWarehouseDefault'], 10, 2);
        add_action('wp_ajax_remove_warehouse', [$this, 'actionWpAjaxRemoveWarehouse'], 10, 2);
        add_action('wp_ajax_create_warehouse', [$this, 'actionWpAjaxCreateWarehouse'], 10, 2);
        add_action('wp_ajax_import_warehouses', [$this, 'actionWpAjaxImportWarehouses'], 10, 2);
        add_action('wp_ajax_export_warehouses', [$this, 'actionWpAjaxExportWarehouses'], 10, 2);
        add_action('wp_ajax_five_post_force_renew_points', [$this, 'actionWpAjaxForseRenewPoints'], 10, 2);
        add_action('woocommerce_after_checkout_validation', [$this, 'actionWoocommerceAfterCheckoutValidation'], 10, 2);

        add_action('add_meta_boxes', [$this, 'actionAddMetaBoxes'], 10);
        add_action('wp_ajax_five_post_selectthisservice', [$this, 'actionWpAjaxFivepostSelectService'], 10);
        add_action('wp_ajax_five_post_update_shipping', [$this, 'actionWpAjaxFivepostUpdateShipping'], 10);
        add_action('wp_ajax_five_post_send_order', [$this, 'actionWpAjaxFivepostSendOrder'], 10);
        add_action('wp_ajax_five_post_cancel_order', [$this, 'actionWpAjaxFivepostCancelOrder'], 10);
        add_action('wp_ajax_five_post_print_label', [$this, 'actionWpAjaxFivepostPrintLabel'], 10);
        add_action('wp_ajax_five_post_get_order_status', [$this, 'actionWpAjaxFivepostGetOrderStatus'], 10);

        add_action('wp_ajax_five_post_save_order_calcdata', [$this, 'actionWpAjaxFivepostSaveOrderCalcdata'], 10);

        add_action('wp_head', [$this, 'addJsConstants']);
        add_action('admin_head', [$this, 'addJsConstants']);
    }

    public function addJsConstants()
    {
        echo '<!----><script>'.
            'window.fivepostPluginUrl = "'. FIVEPOST_PLUGIN_URI .'";'.
        '</script>';
    }

    /**
     * Plugin Install + init Cron
    */
    public function registerActivationHook() {
        global $wpdb;

        $sql = "DROP TABLE IF EXISTS `{$wpdb->base_prefix}fivepost_pickup_points`;
             CREATE TABLE `{$wpdb->base_prefix}fivepost_pickup_points` (
            `point_id` bigint(20) NOT NULL AUTO_INCREMENT,
            `id` varchar(50) NOT NULL,
            `name` varchar(255) NOT NULL,
            `partner_name` varchar(255) NOT NULL,
            `type` varchar(50) NOT NULL,
            `additional` varchar(500) DEFAULT NULL,
            `work_hours` varchar(1000) DEFAULT NULL,
            `full_address` varchar(500) DEFAULT NULL,
            `address_country` varchar(50) DEFAULT NULL,
            `address_zip_code` varchar(10) DEFAULT NULL,
            `address_region` varchar(50) DEFAULT NULL,
            `address_region_type` varchar(50) DEFAULT NULL,
            `address_city` varchar(50) DEFAULT NULL,
            `address_city_type` varchar(10) DEFAULT NULL,
            `address_street` varchar(255) DEFAULT NULL,
            `address_house` varchar(10) DEFAULT NULL,
            `address_building` varchar(10) DEFAULT NULL,
            `latitude` float NOT NULL,
            `longitude` float NOT NULL,
            `metro_station` varchar(100) DEFAULT NULL,
            `max_cell_width` int(11) NOT NULL DEFAULT 0,
            `max_cell_height` int(11) NOT NULL DEFAULT 0,
            `max_cell_length` int(11) NOT NULL DEFAULT 0,
            `max_weight` int(11) NOT NULL DEFAULT 0,
            `return_allowed` tinyint(4) NOT NULL DEFAULT 0,
            `timezone` varchar(100) DEFAULT NULL,
            `phone` varchar(20) DEFAULT NULL,
            `cash_allowed` tinyint(4) NOT NULL DEFAULT 0,
            `card_allowed` tinyint(4) NOT NULL DEFAULT 0,
            `loyalty_allowed` tinyint(4) NOT NULL DEFAULT 0,
            `ext_status` varchar(50) DEFAULT NULL,
            `locality_fias_code` varchar(50) DEFAULT NULL,
            `delivery_sl` int(11) NOT NULL DEFAULT 0,
            `rates` varchar(1000) NOT NULL DEFAULT '[]',
            `last_mile_warehouse_id` varchar(50) NOT NULL DEFAULT '',
            `last_mile_warehouse_name` varchar(100) NOT NULL DEFAULT '',
            `date_sync` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`point_id`)
        ) {$wpdb->get_charset_collate()};";

        $sql .= "DROP TABLE IF EXISTS `{$wpdb->base_prefix}fivepost_warehouses`;
            CREATE TABLE `{$wpdb->base_prefix}fivepost_warehouses` (
            `warehouse_id` bigint(20) NOT NULL AUTO_INCREMENT,
            `uuid` varchar(255) DEFAULT NULL,
            `name` varchar(255) NOT NULL,
            `country_id` varchar(2) NOT NULL DEFAULT 'RU',
            `region_code` int(11) NOT NULL,
            `federal_district` varchar(50) NOT NULL,
            `region` varchar(50) NOT NULL,
            `index` varchar(10) NOT NULL,
            `city` varchar(50) NOT NULL,
            `street` varchar(50) NOT NULL,
            `house_number` varchar(10) NOT NULL,
            `coordinates_x` float NOT NULL,
            `coordinates_y` float NOT NULL,
            `contact_phone_number` varchar(50) NOT NULL,
            `time_zone` varchar(10) NOT NULL,
            `working_time` varchar(1000) NOT NULL,
            `partner_location_id` varchar(100) NOT NULL,
            `is_default` tinyint(4) NOT NULL DEFAULT 0,
            PRIMARY KEY (`warehouse_id`)
        ) {$wpdb->get_charset_collate()};";

        $sql .= "DROP TABLE IF EXISTS `{$wpdb->base_prefix}fivepost_tarifs`;
            CREATE TABLE `{$wpdb->base_prefix}fivepost_tarifs` (
            `tarif_id` bigint(20) NOT NULL AUTO_INCREMENT,
            `tname` varchar(255) NOT NULL,
            PRIMARY KEY (`tarif_id`)
        ) {$wpdb->get_charset_collate()};";

        require_once(ABSPATH.'wp-admin/includes/upgrade.php');

        dbDelta($sql);

        $wpdb->query("ALTER TABLE `{$wpdb->base_prefix}fivepost_pickup_points` ADD UNIQUE KEY `id` (`id`)");
        $wpdb->query("ALTER TABLE `{$wpdb->base_prefix}fivepost_warehouses` ADD UNIQUE KEY `uuid` (`uuid`)");
        $wpdb->query("ALTER TABLE `{$wpdb->base_prefix}fivepost_tarifs` ADD UNIQUE KEY `tname` (`tname`)");
        $wpdb->query("INSERT INTO `{$wpdb->base_prefix}fivepost_tarifs` (`tname`) VALUES ('WP_Auto_Tarif')");

        wp_clear_scheduled_hook('five_post_every_minute_event');
        wp_clear_scheduled_hook('five_post_hourly_event');
        wp_clear_scheduled_hook('five_post_deleteoldfiles_event');
        wp_schedule_event(time(), 'five_post_every_minute', 'five_post_every_minute_event');
        wp_schedule_event(time(), 'hourly', 'five_post_hourly_event');
        wp_schedule_event(time(), 'daily', 'five_post_deleteoldfiles_event');
        delete_option('fivepost-page-number');
        delete_option('fivepost-update-date');
    }

    /**
     * Uninstall Plugin
     */
    public function registerDeactivationHook() {
        global $wpdb;

        $wpdb->query("TRUNCATE TABLE `{$wpdb->base_prefix}fivepost_pickup_points`");
        $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->base_prefix}fivepost_pickup_points`");
        $wpdb->query("TRUNCATE TABLE `{$wpdb->base_prefix}fivepost_warehouses`");
        $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->base_prefix}fivepost_warehouses`");
        $wpdb->query("TRUNCATE TABLE `{$wpdb->base_prefix}fivepost_tarifs`");
        $wpdb->query("DROP TABLE IF EXISTS `{$wpdb->base_prefix}fivepost_tarifs`");

        wp_clear_scheduled_hook('five_post_every_minute_event');
        wp_clear_scheduled_hook('five_post_hourly_event');
        wp_clear_scheduled_hook('five_post_deleteoldfiles_event');
        delete_option('fivepost-page-number');
        delete_option('fivepost-update-date');
    }

    /**
     * Add settings button in admin panel
     *
     * @param array $links
     * @return array
     */
    public function filterPluginActionLinks(array $links): array {
        $new_links[] = '<a href="'.admin_url( 'admin.php?page=wc-settings&tab=shipping&section=fivepost_shipping_method').'">'.
            __('Settings').
            '</a>';

        return array_merge($new_links, $links);
    }

    /**
     * Handler new ver plugin
    */
    public function filterPluginsApi($false, $action, $arg) {

    }

    /**
     * Add shedule for cron task - every 1 min
    */
    public function filterCronSchedules($schedules) {
        if(!isset($schedules['five_post_every_minute']))
            $schedules['five_post_every_minute'] = array(
                'interval' => 60,
                'display' => __('Every minute', 'fivepost-wp')
            );
        return $schedules;
    }

    /**
     * Register delivering method in WP
    */
    public function filterShippingMethods($methods) {
        $methods[] = 'Fivepost_Shipping_Method';
        return $methods;
    }

    /**
     * Init lang files
    */
    public function actionInit() {
        load_textdomain('fivepost-wp',FIVEPOST_PLUGIN_DIR.'/languages/fivepost-wp-ru_RU.mo');
    }

    /**
     * Init CRON tasks
    */
    public function actionWP() {
        if(!wp_next_scheduled('five_post_every_minute_event'))
            wp_schedule_event(time(), 'five_post_every_minute', 'five_post_every_minute_event');
        if(!wp_next_scheduled('five_post_hourly_event'))
            wp_schedule_event(time(), 'hourly', 'five_post_hourly_event');
        if(!wp_next_scheduled('five_post_deleteoldfiles_event'))
            wp_schedule_event(time(), 'daily', 'five_post_deleteoldfiles_event');
    }

    /**
     * Loading & update 5P Points - cron event - every 1 min
    */
    public function actionEveryMinuteEvent() {
        if(get_option('fivepost-update-date', null) != date('Y-m-d')) {
            require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

            $shipping_method = new \Fivepost_Shipping_Method();

            if($shipping_method->has_settings() && $shipping_method->get_option('api_key')) {
                $adapter = new CurlAdapter();
                $sdk     = new Sdk($adapter, '', null, $shipping_method->get_option('api_wtype'));
                $request = new JwtGenerate();
                $request->setApikey($shipping_method->get_option('api_key'));

                $jwt = $sdk->jwtGenerate($request)->getResponse();

                $adapter = new CurlAdapter();
                $sdk     = new Sdk($adapter, $jwt->getJwt(), null, $shipping_method->get_option('api_wtype'));

                $page_number = get_option('fivepost-page-number', 0);
                $request = new PickupPoints();
                $request->setPageNumber($page_number)->setPageSize(1000);

                $result = $sdk->pickupPoints($request)->getResponse();

                if($result->getSuccess() && !empty($result->getContent()->getFields())) {
                    $values = [];

                    foreach ($result->getContent()->getFields() as $point) {
                        if(empty($point['rate'])) continue;
                        $values[] = [
                            'id'=>$point['id'],
                            'name'=>addslashes($point['name']),
                            'partner_name'=>addslashes($point['partnerName']),
                            'type'=>addslashes($point['type']),
                            'additional'=>(!empty($point['additional']) ? addslashes($point['additional']) : ''),
                            'work_hours'=>!empty($point['workHours']) ? json_encode($point['workHours']) : '[]',
                            'full_address'=>!empty($point['fullAddress']) ? addslashes($point['fullAddress']) : '',
                            'address_country'=>(!empty($point['address']) && !empty($point['address']['country'])) ? addslashes($point['address']['country']) : '',
                            'address_zip_code'=>(!empty($point['address']) && !empty($point['address']['zipCode'])) ? addslashes($point['address']['zipCode']) : '',
                            'address_region'=>(!empty($point['address']) && !empty($point['address']['region'])) ? addslashes($point['address']['region']) : '',
                            'address_region_type'=>(!empty($point['address']) && !empty($point['address']['regionType'])) ? addslashes($point['address']['regionType']) : '',
                            'address_city'=>(!empty($point['address']) && !empty($point['address']['city'])) ? addslashes($point['address']['city']) : '',
                            'address_city_type'=>(!empty($point['address']) && !empty($point['address']['cityType'])) ? addslashes($point['address']['cityType']) : '',
                            'address_street'=>(!empty($point['address']) && !empty($point['address']['street'])) ? addslashes($point['address']['street']) : '',
                            'address_house'=>(!empty($point['address']) && !empty($point['address']['house'])) ? addslashes($point['address']['house']) : '',
                            'address_building'=>(!empty($point['address']) && !empty($point['address']['building'])) ? addslashes($point['address']['building']) : '',
                            'latitude'=>(!empty($point['address']) && !empty($point['address']['lat'])) ? floatval($point['address']['lat']) : 0,
                            'longitude'=>(!empty($point['address']) && !empty($point['address']['lng'])) ? floatval($point['address']['lng']) : 0,
                            'metro_station'=>(!empty($point['address']) && !empty($point['address']['metroStation'])) ? addslashes($point['address']['metroStation']) : '',
                            'max_cell_width'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxCellWidth'])) ? intval($point['cellLimits']['maxCellWidth']) : 0,
                            'max_cell_height'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxCellHeight'])) ? intval($point['cellLimits']['maxCellHeight']) : 0,
                            'max_cell_length'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxCellLength'])) ? intval($point['cellLimits']['maxCellLength']) : 0,
                            'max_weight'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxWeight'])) ? intval($point['cellLimits']['maxWeight']) : 0,
                            'return_allowed'=>!empty($point['returnAllowed']) ? boolval($point['returnAllowed']) : 0,
                            'timezone'=>!empty($point['timezone']) ? addslashes($point['timezone']) : '',
                            'phone'=>!empty($point['phone']) ? addslashes($point['phone']) : '',
                            'cash_allowed'=>!empty($point['cashAllowed']) ? boolval($point['cashAllowed']) : 0,
                            'card_allowed'=>!empty($point['cardAllowed']) ? boolval($point['cardAllowed']) : 0,
                            'loyalty_allowed'=>!empty($point['loyaltyAllowed']) ? boolval($point['loyaltyAllowed']) : 0,
                            'ext_status'=>!empty($point['extStatus']) ? addslashes($point['extStatus']) : '',
                            'locality_fias_code'=>!empty($point['localityFiasCode']) ? addslashes($point['localityFiasCode']) : '',
                            'delivery_sl'=>(!empty($point['deliverySL']) && !empty($point['deliverySL'][0]) && !empty($point['deliverySL'][0]['Sl'])) ? intval($point['deliverySL'][0]['Sl']) : 0,
                            'rates'=>!empty($point['rate']) ? json_encode($point['rate']) : '[]',
                            'last_mile_warehouse_id'=>(!empty($point['lastMileWarehouse']) && !empty($point['lastMileWarehouse']['id'])) ? addslashes($point['lastMileWarehouse']['id']) : '',
                            'last_mile_warehouse_name'=>(!empty($point['lastMileWarehouse']) && !empty($point['lastMileWarehouse']['name'])) ? addslashes($point['lastMileWarehouse']['name']) : '',
                            'date_sync'=>date('Y-m-d H:i:s')
                        ];

                        if (!empty($point['rate'])) {
                            global $wpdb;
                            foreach ($point['rate'] as $r) {
                                $row = $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}fivepost_tarifs` WHERE `tname` = %s;",$r['rateType']));
                                if (!$row) $wpdb->insert($wpdb->base_prefix.'fivepost_tarifs',['tname'=>$r['rateType']]);
                            }
                        }
                    }
                    $coltypes=[
                        '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%f','%f','%s','%d','%d','%d','%d','%d','%s','%s','%d','%d','%d','%s','%s','%d','%s','%s','%s','%s'
                    ];
                    if(!empty($values)) { //correct bugs in points
                        global $wpdb;
                        for ($i=0;$i<=count($values)-1;$i++) {
                            foreach ($values[$i] as &$vl) {
                                $vl = preg_replace('/\r/','',$vl);
                                $vl = preg_replace('/\n/','',$vl);
                            }
                            unset($vl);
                        }
                        foreach ($values as $val) $wpdb->replace($wpdb->base_prefix.'fivepost_pickup_points',$val,$coltypes);
                    }

                    update_option('fivepost-page-number', $page_number + 1);
                } else {
                    update_option('fivepost-page-number', 0);
                    update_option('fivepost-update-date', date('Y-m-d'));
                }
            }
        }
    }

    public function actionWpAjaxForseRenewPoints() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');
        $shipping_method = new \Fivepost_Shipping_Method();
        if($shipping_method->has_settings() && $shipping_method->get_option('api_key')) {
            if (!isset($_POST['pgn'])) {
                wp_send_json([
                    'ans' => false,
                    'code'=>-3 //pgn not defined
                ]);
                wp_die();
            }
            $page_number = intval($_POST['pgn']);

            $adapter = new CurlAdapter();
            $sdk     = new Sdk($adapter, '', null, $shipping_method->get_option('api_wtype'));
            $request = new JwtGenerate();
            $request->setApikey($shipping_method->get_option('api_key'));
            $jwt = $sdk->jwtGenerate($request)->getResponse();
            $adapter = new CurlAdapter();
            $sdk     = new Sdk($adapter, $jwt->getJwt(), null, $shipping_method->get_option('api_wtype'));
            $request = new PickupPoints();
            $request->setPageNumber($page_number)->setPageSize(1000);
            $result = $sdk->pickupPoints($request)->getResponse();

            if($result->getSuccess() && !empty($result->getContent()->getFields())) {
                $values = [];
                $decoded = $result->getDecoded();
                $curpg = $decoded->number;
                $totalpgs = $decoded->totalPages;
                $is_first = $decoded->first;
                $is_last = $decoded->last;

                foreach ($result->getContent()->getFields() as $point) {
                    if(empty($point['rate'])) continue;
                    $values[] = [
                        'id'=>$point['id'],
                        'name'=>addslashes($point['name']),
                        'partner_name'=>addslashes($point['partnerName']),
                        'type'=>addslashes($point['type']),
                        'additional'=>(!empty($point['additional']) ? addslashes($point['additional']) : ''),
                        'work_hours'=>!empty($point['workHours']) ? json_encode($point['workHours']) : '[]',
                        'full_address'=>!empty($point['fullAddress']) ? addslashes($point['fullAddress']) : '',
                        'address_country'=>(!empty($point['address']) && !empty($point['address']['country'])) ? addslashes($point['address']['country']) : '',
                        'address_zip_code'=>(!empty($point['address']) && !empty($point['address']['zipCode'])) ? addslashes($point['address']['zipCode']) : '',
                        'address_region'=>(!empty($point['address']) && !empty($point['address']['region'])) ? addslashes($point['address']['region']) : '',
                        'address_region_type'=>(!empty($point['address']) && !empty($point['address']['regionType'])) ? addslashes($point['address']['regionType']) : '',
                        'address_city'=>(!empty($point['address']) && !empty($point['address']['city'])) ? addslashes($point['address']['city']) : '',
                        'address_city_type'=>(!empty($point['address']) && !empty($point['address']['cityType'])) ? addslashes($point['address']['cityType']) : '',
                        'address_street'=>(!empty($point['address']) && !empty($point['address']['street'])) ? addslashes($point['address']['street']) : '',
                        'address_house'=>(!empty($point['address']) && !empty($point['address']['house'])) ? addslashes($point['address']['house']) : '',
                        'address_building'=>(!empty($point['address']) && !empty($point['address']['building'])) ? addslashes($point['address']['building']) : '',
                        'latitude'=>(!empty($point['address']) && !empty($point['address']['lat'])) ? floatval($point['address']['lat']) : 0,
                        'longitude'=>(!empty($point['address']) && !empty($point['address']['lng'])) ? floatval($point['address']['lng']) : 0,
                        'metro_station'=>(!empty($point['address']) && !empty($point['address']['metroStation'])) ? addslashes($point['address']['metroStation']) : '',
                        'max_cell_width'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxCellWidth'])) ? intval($point['cellLimits']['maxCellWidth']) : 0,
                        'max_cell_height'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxCellHeight'])) ? intval($point['cellLimits']['maxCellHeight']) : 0,
                        'max_cell_length'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxCellLength'])) ? intval($point['cellLimits']['maxCellLength']) : 0,
                        'max_weight'=>(!empty($point['cellLimits']) && !empty($point['cellLimits']['maxWeight'])) ? intval($point['cellLimits']['maxWeight']) : 0,
                        'return_allowed'=>!empty($point['returnAllowed']) ? boolval($point['returnAllowed']) : 0,
                        'timezone'=>!empty($point['timezone']) ? addslashes($point['timezone']) : '',
                        'phone'=>!empty($point['phone']) ? addslashes($point['phone']) : '',
                        'cash_allowed'=>!empty($point['cashAllowed']) ? boolval($point['cashAllowed']) : 0,
                        'card_allowed'=>!empty($point['cardAllowed']) ? boolval($point['cardAllowed']) : 0,
                        'loyalty_allowed'=>!empty($point['loyaltyAllowed']) ? boolval($point['loyaltyAllowed']) : 0,
                        'ext_status'=>!empty($point['extStatus']) ? addslashes($point['extStatus']) : '',
                        'locality_fias_code'=>!empty($point['localityFiasCode']) ? addslashes($point['localityFiasCode']) : '',
                        'delivery_sl'=>(!empty($point['deliverySL']) && !empty($point['deliverySL'][0]) && !empty($point['deliverySL'][0]['Sl'])) ? intval($point['deliverySL'][0]['Sl']) : 0,
                        'rates'=>!empty($point['rate']) ? json_encode($point['rate']) : '[]',
                        'last_mile_warehouse_id'=>(!empty($point['lastMileWarehouse']) && !empty($point['lastMileWarehouse']['id'])) ? addslashes($point['lastMileWarehouse']['id']) : '',
                        'last_mile_warehouse_name'=>(!empty($point['lastMileWarehouse']) && !empty($point['lastMileWarehouse']['name'])) ? addslashes($point['lastMileWarehouse']['name']) : '',
                        'date_sync'=>date('Y-m-d H:i:s')
                    ];

                    if (!empty($point['rate'])) {
                        global $wpdb;
                        foreach ($point['rate'] as $r) {
                            $row = $wpdb->get_row($wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}fivepost_tarifs` WHERE `tname` = %s;",$r['rateType']));
                            if (!$row) $wpdb->insert($wpdb->base_prefix.'fivepost_tarifs',['tname'=>$r['rateType']]);
                        }
                    }
                }
                $coltypes=[
                    '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%f','%f','%s','%d','%d','%d','%d','%d','%s','%s','%d','%d','%d','%s','%s','%d','%s','%s','%s','%s'
                ];
                if(!empty($values)) { //correct bugs in points
                    global $wpdb;
                    for ($i=0;$i<=count($values)-1;$i++) {
                        foreach ($values[$i] as &$vl) {
                            $vl = preg_replace('/\r/','',$vl);
                            $vl = preg_replace('/\n/','',$vl);
                        }
                        unset($vl);
                    }
                    foreach ($values as $val) $wpdb->replace($wpdb->base_prefix.'fivepost_pickup_points',$val,$coltypes);
                }

                wp_send_json([
                    'ans' => true,
                    'isf' => $is_first,
                    'isl' => $is_last,
                    'pgn' => $curpg+1,
                    'tot' => $totalpgs
                ]);

            } else {
                wp_send_json([
                    'ans' => false,
                    'code'=>-2 //Error or NoData
                ]);
                wp_die();
            }


        } else {
            wp_send_json([
                'ans' => false,
                'code'=>-1 //not configured
            ]);
            wp_die();
        }



        wp_send_json([
            'ans' => true
        ]);
        wp_die();
    }

    /**
     * Cron event for update order statuses - every 1 hour
     */
    public function actionHourlyEvent() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

        $shipping_method = new \Fivepost_Shipping_Method();
        $begin_date = (new \DateTime())->modify('-2 weeks');
        $final_date = new \DateTime();

        $orders = wc_get_orders([
            'limit' => -1,
            'type'=> 'shop_order',
            'date_created' => $begin_date->format('Y-m-d').'...'.$final_date->format('Y-m-d'),
            'order' => 'date_created DESC'
        ]);

        $adapter = new CurlAdapter();
        $sdk = new Sdk($adapter, '', null, $shipping_method->get_option('api_wtype'));
        $request = new JwtGenerate();
        $request->setApikey($shipping_method->get_option('api_key'));
        $jwt = $sdk->jwtGenerate($request)->getResponse();

        foreach($orders as $order) {
            if($fivepost_order_id = $order->get_meta('_fivepost_order_id')) {
                $adapter = new CurlAdapter();
                $sdk = new Sdk($adapter, $jwt->getJwt(), null, $shipping_method->get_option('api_wtype'));
                $get_order_history = new GetOrderHistory();
                $get_order_history->setOrderId($fivepost_order_id);
                $response = $sdk->getOrderHistory($get_order_history)->getResponse();

                if(method_exists($response, 'getHistory') && is_array($response->getHistory()->getFields())) {
                    $history = $response->getHistory()->getFields();

                    if(!empty($history) && is_array($history)) {
                        $status = array_pop($history);

                        if(!empty($status['status'])) {
                            $new_orderstatus = $shipping_method->map_statuses($status['status'],$status['execstatus']);
                            //Change Order Status...
                            if ($new_orderstatus) {
                                $order->update_status($new_orderstatus);
                                $order->save();
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Cron - DeleteOldFiles PrintLabels - Start every day
     */
    public function actionDeleteoldfilesEvent() {
        global $wpdb;
        $files_dir = FIVEPOST_PLUGIN_DIR.'files';
        $files = scandir($files_dir);
        $now=time();
        $now += (3600*24*7);
        foreach ($files as $file) {
            if ($file=='.') continue;
            if ($file=='..') continue;
            if (($now - filemtime($files_dir.'/'.$file)>(3600*24*7))) {
                unlink($files_dir.'/'.$file);
                $order = $wpdb->get_row(
                        $wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}postmeta` WHERE `meta_key` = '_fivepost_order_id_labelprintfile' AND `meta_value` = %s;",$file)
                );
                if ($order) delete_post_meta((int)$order->post_id,'_fivepost_order_id_labelprintfile');
            }
        }
    }

    /**
     * Init frontend form creating order - adding css style files for form fields
     */
    public function filterWoocommerceCheckoutFields($fields) {
        $fields['billing']['billing_city']['class'][] = 'update-on-change';
        $fields['shipping']['shipping_city']['class'][] = 'update-on-change';
        $fields['billing']['billing_state']['class'][] = 'update-on-change';
        $fields['shipping']['shipping_state']['class'][] = 'update-on-change';
        $fields['billing']['billing_postcode']['class'][] = 'update-on-change';
        $fields['shipping']['shipping_postcode']['class'][] = 'update-on-change';

        return $fields;
    }

    /**
     * Add custom html for 5P map on ADMIN - edit order page
    */
    public function actionAdminFooter() {?>
        <div class="post5-popup" id="post5-map-popup" data-map="post5-map">
            <button type="button" class="post5-popup-close" title="<?php _e('Close', 'fivepost-wp')?>">&times;</button>
            <div class="post5-popup-header"><?php _e('Choose pickup point', 'fivepost-wp')?></div>
            <div class="post5-popup-content">
                <div class="yandex-map" id="post5-map"></div>
            </div>
        </div>
    <?php
    }

    /**
     * Adding custom html for 5P points map on FRONT in create-order-page
     */
    public function actionWPFooter() {
        if(!is_checkout()) return;?>
            <div class="post5-popup" id="post5-map-popup" data-map="post5-map">
                <button type="button" class="post5-popup-close" title="<?php _e('Close', 'fivepost-wp')?>">&times;</button>
                <div class="post5-popup-header"><?php _e('Choose pickup point', 'fivepost-wp')?></div>
                <div class="post5-popup-content">
                    <div class="yandex-map" id="post5-map"></div>
                </div>
            </div>
            <script type="text/javascript">
                jQuery(document).on('change', '.update-on-change', function(e) {
                    jQuery('#fivepost_point_id').val('');
                    jQuery('#fivepost_point_zone').val('');
                    jQuery(document.body).trigger('update_checkout');
                });
            </script>
        <?php
    }

    /**
     * Init class for 5P shipping method
    */
    public function actionCourierShippingInit() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');
    }

    public function actionAfterShippingRate($method, $index) { //second
        if(!is_checkout()) return;
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

        $shipping_method = new \Fivepost_Shipping_Method();

        if($method->id != $shipping_method->id.':pickup') return;

        $chosen_method_id = WC()->session->chosen_shipping_methods[$index];

        if($chosen_method_id == $shipping_method->id.':pickup'):
            parse_str(!empty($_POST['post_data']) ? sanitize_text_field($_POST['post_data']) : '', $post_data);

            woocommerce_form_field( 'fivepost_point_id' , array(
                'type'          => 'hidden',
                'required'      => true
            ), !empty($post_data['fivepost_point_id']) ? $post_data['fivepost_point_id'] : '');

            woocommerce_form_field( 'fivepost_point_zone' , array(
                'type'          => 'hidden',
                'required'      => true
            ), !empty($post_data['fivepost_point_zone']) ? $post_data['fivepost_point_zone'] : '');

            woocommerce_form_field( 'fivepost_point_pay_cash' , array(
                'type'          => 'hidden',
                'required'      => true
            ), !empty($post_data['fivepost_point_pay_cash']) ? $post_data['fivepost_point_pay_cash'] : '');
            woocommerce_form_field( 'fivepost_point_pay_card' , array(
                'type'          => 'hidden',
                'required'      => true
            ), !empty($post_data['fivepost_point_pay_card']) ? $post_data['fivepost_point_pay_card'] : '');
        endif;
    }

    public function actionWoocommerceCheckoutUpdateOrderReview() { //first
        $packages = WC()->cart->get_shipping_packages();

        foreach ($packages as $key => $value) {
            $shipping_session = "shipping_for_package_$key";

            unset(WC()->session->$shipping_session);
        }
    }

    /**
     * Collection 5P points for further placing them on the map when placing an order.
     *
     * @param string $label
     * @param \WC_Shipping_Rate $method
     * @return string
     */
    public function actionCartShippingMethodFullLabel(string $label, \WC_Shipping_Rate $method): string {
        if(function_exists('is_checkout') && is_checkout()) {
            if ($method->id === 'fivepost_shipping_method:pickup') {
                require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');
                $shipping_method = new \Fivepost_Shipping_Method();
                $meta_data = $method->get_meta_data();
                if ($meta_data) {
                    $script = '<script>window.pickupPoints = [],window.fivepost_systemdlv_cash=[],window.fivepost_systemdlv_card=[];';
                    $days = [
                        'MON' => __('Monday', 'fivepost-wp'),
                        'TUE' => __('Tuesday', 'fivepost-wp'),
                        'WED' => __('Wednesday', 'fivepost-wp'),
                        'THU' => __('Thursday', 'fivepost-wp'),
                        'FRI' => __('Friday', 'fivepost-wp'),
                        'SAT' => __('Saturday', 'fivepost-wp'),
                        'SUN' => __('Sunday', 'fivepost-wp'),
                    ];
                    if (is_array($shipping_method->settings['online_payments'])) foreach ($shipping_method->settings['online_payments'] as $pm) $script.='fivepost_systemdlv_card.push(\''.$pm.'\');';
                    if (is_array($shipping_method->settings['npp_payments'])) foreach ($shipping_method->settings['npp_payments'] as $pm) $script.='fivepost_systemdlv_cash.push(\''.$pm.'\');';
                    foreach($meta_data['points'] as $point) {
                        $work_hours = @json_decode($point->work_hours);
                        $schedule = [];
                        $payment_methods = [];
                        $delivery_price = 0;

                        if (!isset($point->zone)) continue;

                        if(!empty($meta_data['rates'][$point->zone]))
                            $delivery_price = $meta_data['rates'][$point->zone]->delivery_price;

                        if(!$delivery_price)
                            continue;

                        foreach ($work_hours as $item) {
                            if(isset($days[$item->day])) {
                                $time_start = new \DateTime($item->opensAt);
                                $time_end = new \DateTime($item->closesAt);
                                $schedule[] = '<div class="point-worktime-day">'.
                                    $days[$item->day].': '.$time_start->format('H:i').' - '.$time_end->format('H:i').
                                    '</div>';
                            }
                        }

                        if($point->cash_allowed)
                            $payment_methods[] = __('Cash payment', 'fivepost-wp');

                        if($point->card_allowed)
                            $payment_methods[] = __('Card payment', 'fivepost-wp');

                        if($point->loyalty_allowed)
                            $payment_methods[] = __('Loyalty payment', 'fivepost-wp');

                        if (!$point->cash_allowed && !$point->card_allowed && !$point->loyalty_allowed) {
                            $payment_methods[] = __('No payment', 'fivepost-wp');
                        }

                        $dlv_time = intval($point->delivery_sl)+intval($shipping_method->settings['add_dlvtime']);
                        $popup_content =
                            '<div class="point-address-wrapper'.($point->cash_allowed?' pcash':'').($point->card_allowed?' pcard':'').'">'.
                            '<div class="point-address-content">'.$point->full_address.'</div>'.
                            '</div>'.
                            '<div class="point-worktime-wrapper" style="margin-top: 10px;">'.
                            '<div class="point-worktime-title"><strong>'.__('Work time', 'fivepost-wp').':</strong></div>'.
                            '<div class="point-worktime-content">'.implode('', $schedule).'</div>'.
                            '</div>'.
                            '<div class="point-description-wrapper" style="margin-top: 10px">'.
                            '<div class="point-description-title"><strong>'.__('Description:', 'fivepost-wp').'</strong></div>'.
                            '<div class="point-description-content">'.$point->additional.'</div>'.
                            '</div>'.
                            '<div class="point-payment-wrapper" style="margin-top: 10px;">'.
                            '<div class="point-payment-title"><strong>'.__('Payment Methods:', 'fivepost-wp').'</strong></div>'.
                            '<div class="point-payment-content" style="font-weight: bold">'.implode(', ', $payment_methods).'</div>'.
                            '</div>'.
                            '<div class="point-total-wrapper" style="margin-top: 10px;"><strong>'.__('Delivery time', 'fivepost-wp').': </strong>'.\Ipol\Fivepost\WordPress\Tools\Tools::declination($dlv_time, [__('day', 'fivepost-wp'), _x('days','2_days', 'fivepost-wp'), _x('days','many_days', 'fivepost-wp')]).'</div>'.
                            '<div class="point-total-wrapper" style="margin-top: 10px;">'.
                            '<strong>'.__('Price:', 'fivepost-wp').'</strong> '.wc_price($delivery_price).
                            '</div>';

                        $icon_url = FIVEPOST_PLUGIN_URI.'assets/images/png/'.$point->type;
                        $script .= 'pickupPoints.push({
                      id: \''.$point->id.'\',
                      name: \''.$point->name.'\',
                      zone: \''.$point->zone.'\',
                      address: \''.$point->full_address.'\',
                      latitude: \''.$point->latitude.'\',
                      longitude: \''.$point->longitude.'\',
                      iconUrl: \''.$icon_url.'\',
                      popupContent: \''.$popup_content.'\',
                      pcash:"'.($point->cash_allowed?'1':'0').'",
                      pcard:"'.($point->card_allowed?'1':'0').'",
                      sl:"'.$dlv_time.'"
                    });';
                    }

                    $script .= '</script>';

                    $label = $label . '<span class="post5-map-btn-wrapper"><button type="button" class="button alt button-map" data-post5_popup="post5-map-popup">' .
                        __('Select on map', 'fivepost-wp') .
                        '</button></span>'.$script;
                }
            }
        }

        return $label;
    }

    /**
     * Adding CSS & JS on ADMIN
    */
    public function actionAdminEnqueueScripts() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

        $shipping_method = new \Fivepost_Shipping_Method();

        if($shipping_method->has_settings() && $shipping_method->get_option('yandex_maps_key'))
            wp_enqueue_script('yandex-maps-admin', '//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey='.$shipping_method->get_option('yandex_maps_key'), array(), FIVEPOST_PLUGIN_VERSION, true);

        wp_enqueue_script('fivepost-wp'.'-inputmask', FIVEPOST_PLUGIN_URI.'assets/js/jquery.inputmask.min.js', array(
            'jquery'
        ), FIVEPOST_PLUGIN_VERSION, true);
        wp_enqueue_script('fivepost-wp'.'-scripts', FIVEPOST_PLUGIN_URI.'assets/js/admin-scripts.js', array(
            'jquery', 'yandex-maps-admin', 'fivepost-wp'.'-inputmask'
        ), FIVEPOST_PLUGIN_VERSION, true);
        wp_localize_script('fivepost-wp'.'-scripts', 'wp_ajax',
            array(
                'url' => admin_url('admin-ajax.php'),
            )
        );
        wp_localize_script('fivepost-wp'.'-scripts', 'fivepost_localization',
            array(
                'error_warehouses_load' => __('Failed to load warehouse information.', 'fivepost-wp'),
                'error_warehouses_set_default' => __('Failed to set warehouse as default.', 'fivepost-wp'),
                'error_warehouses_set_default_question' => __('Are you sure you want to set this warehouse as default?', 'fivepost-wp'),
                'error_warehouses_remove' => __('Failed to remove warehouse.', 'fivepost-wp'),
                'error_warehouses_remove_question' => __('Are you sure you want to remove this warehouse?', 'fivepost-wp'),
                'error_warehouses_create' => __('Failed to create warehouse.', 'fivepost-wp'),
                'error_warehouses_import' => __('Failed to import warehouses information.', 'fivepost-wp'),
            )
        );
        wp_enqueue_style('fivepost-wp'.'-admin-styles', FIVEPOST_PLUGIN_URI.'assets/css/admin-styles.css', array(), FIVEPOST_PLUGIN_VERSION);
    }

    /**
     * Adding CSS & JS on FRONT
     */
    public function actionWpEnqueueScripts() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

        $shipping_method = new \Fivepost_Shipping_Method();

        if($shipping_method->has_settings() && $shipping_method->get_option('yandex_maps_key'))
            wp_enqueue_script('yandex-maps', '//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey='.$shipping_method->get_option('yandex_maps_key'), array(), FIVEPOST_PLUGIN_VERSION, true);

        wp_enqueue_script('fivepost-wp'.'-scripts', FIVEPOST_PLUGIN_URI.'assets/js/scripts.js?v3', array('jquery', 'yandex-maps'), FIVEPOST_PLUGIN_VERSION, true);
        wp_enqueue_style('fivepost-wp'.'-styles', FIVEPOST_PLUGIN_URI.'assets/css/styles.css', array(), FIVEPOST_PLUGIN_VERSION);
    }

    /**
     * Additional rules for form validation on create order
    */
    public function actionWoocommerceAfterCheckoutValidation($fields, $errors) {
        if(in_array('fivepost_shipping_method:pickup', $fields['shipping_method'])) {
            require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');
            $shipping_method = new \Fivepost_Shipping_Method();
            //if ... then checking select point and paysystem
            if ($shipping_method->settings['enshp_force']=='yes') {
                if(empty($_POST['fivepost_point_id']) || empty($_POST['fivepost_point_zone'])) {
                    $errors->add('validation', __('Please, choose point on map', 'fivepost-wp'));
                    return;
                }
                //checking pay systems
                $pay_in_card = in_array($_POST['payment_method'],$shipping_method->settings['online_payments']);
                $pay_in_cash = in_array($_POST['payment_method'],$shipping_method->settings['npp_payments']);
                if ($pay_in_card) { //Point must be support CardPay
                    if ($_POST['fivepost_point_pay_card']!=='1') $errors->add('validation', __('This pickup point does not accept card payments. Please choose another payment system or pickup point 5Post.', 'fivepost-wp'));
                }
                if ($pay_in_cash) {
                    if ($_POST['fivepost_point_pay_cash']!=='1') $errors->add('validation', __('This pickup point does not accept cash payments. Please choose another payment system or pickup point 5Post.', 'fivepost-wp'));
                }
            }
        }
    }

    /**
     * Saving additional meta about 5P Point after creating order
    */
    public function actionWoocommerceCheckoutCreateOrder($order, $data) {
        if(!empty($_POST['fivepost_point_id'])) $order->update_meta_data('_fivepost_point_id', sanitize_text_field($_POST['fivepost_point_id']));
        else $order->update_meta_data('_fivepost_point_id', 'NoPoint');
    }

    public function actionWpAjaxLoadWarehousesForm() {
        $response = [
            'html' => ''
        ];

        global $wpdb;

        $warehouses = $wpdb->get_results("SELECT * FROM `{$wpdb->base_prefix}fivepost_warehouses`");
        $days = [
            1 => __('Monday', 'fivepost-wp'),
            2 => __('Tuesday', 'fivepost-wp'),
            3 => __('Wednesday', 'fivepost-wp'),
            4 => __('Thursday', 'fivepost-wp'),
            5 => __('Friday', 'fivepost-wp'),
            6 => __('Saturday', 'fivepost-wp'),
            7 => __('Sunday', 'fivepost-wp'),
        ];

        ob_start();?>
        <h3 class="wc-settings-sub-title " id="woocommerce_fivepost_warehouses_title">
            <?php _e('Warehouses', 'fivepost-wp')?>
        </h3>
        <?php echo wp_kses(\Ipol\Fivepost\WordPress\Tools\Tools::render('warehouses'),[
                'div'=>[
                        'class'=>[]
                ],
                'br'=>[],
                'p'=>[
                        'class'=>[]
                ]
        ])  ?>
        <?php if(!empty($warehouses)):?>
            <div class="warehouses-list">
                <?php foreach ($warehouses as $warehouse):
                    $working_time = @json_decode($warehouse->working_time)?>
                    <div class="card" id="warehouse-<?php echo esc_attr($warehouse->warehouse_id)?>">
                        <h2 class="title"><?php echo esc_html($warehouse->name) ?> <?php echo esc_html($warehouse->is_default ? __('(configured as default)','fivepost-wp') : '') ?></h2>
                        <p>
                            <strong><?php _e('Address', 'fivepost-wp')?></strong>:
                            <?php echo esc_html(!empty($warehouse->country_id) ? $warehouse->country_id.', ' : '') ?>
                            <?php echo esc_html($warehouse->federal_district) ?>,
                            <?php echo esc_html($warehouse->index)?>,
                            <?php echo esc_html($warehouse->city)?>,
                            <?php echo esc_html($warehouse->street)?>,
                            <?php echo esc_html($warehouse->house_number)?>
                        </p>
                        <p>
                            <strong><?php _e('Phone', 'fivepost-wp')?></strong>:
                            <?php echo esc_html($warehouse->contact_phone_number)?>
                        </p>
                        <?php if($working_time):
                            $working_time_rows = [];
                        ?>
                            <?php foreach($working_time as $day):
                                if(empty($day->dayNumber) || empty($day->timeFrom) || empty($day->timeTill) || !isset($days[$day->dayNumber]))
                                    continue;

                                $working_time_rows[$day->dayNumber] = '<strong>'.$days[$day->dayNumber].'</strong>: '.
                                    '<span>'.__('from', 'fivepost-wp').' '.$day->timeFrom.' '.
                                    __('to', 'fivepost-wp').' '.$day->timeTill.'</span>';
                            ?>

                            <?php endforeach;?>
                            <p>
                                <strong><?php _e('Work time', 'fivepost-wp')?></strong>:<br />
                                <?php echo wp_kses(implode('<br />', $working_time_rows),[
                                        'br'=>[]
                                ])?>
                            </p>

                        <?php endif;?>
                        <p class="submit">
                            <button class="button-primary set-warehouse-default"
                                    type="button"<?php echo esc_attr($warehouse->is_default ? ' disabled' : '') ?>
                                    data-warehouse="<?php echo esc_attr($warehouse->warehouse_id) ?>"
                            >
                                <?php _e('Set as default', 'fivepost-wp')?>
                            </button>
                            <button class="button-primary remove-warehouse"
                                    type="button"<?php echo esc_attr($warehouse->is_default ? ' disabled' : '')?>
                                    data-warehouse="<?php echo esc_attr($warehouse->warehouse_id)?>">
                                <?php _e('Remove from list', 'fivepost-wp')?>
                            </button>
                        </p>
                    </div>
                <?php endforeach;?>
            </div>
        <?php else:?>
            <div class="notice notice-warning inline">
                <p><strong><?php _e('No warehouse was found.', 'fivepost-wp')?></strong></p>
            </div>
        <?php endif;?>
        <form method="post" id="warehousesform" action="" enctype="multipart/form-data">
            <input type="hidden" name="action" id="woocommerce_fivepost_warehouses_action" value="" />
            <p class="submit">
                <button class="button-primary" type="button" id="show-create-warehouse">
                    <?php _e('Create new warehouse', 'fivepost-wp')?>
                </button>
            </p>
            <div id="create-warehouse-wrapper" style="display: none">
                <table class="form-table">
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_name">
                                <?php _e('Warehouse name', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Name', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_name"
                                       id="woocommerce_fivepost_create_warehouse_name" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_country_id">
                                <?php _e('Country id', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Country id', 'fivepost-wp')?></span>
                                </legend>
                                <select name="woocommerce_fivepost_create_warehouse_country_id"
                                        id="woocommerce_fivepost_create_warehouse_country_id"
                                        style="min-width: 350px;" class="wc-enhanced-select">
                                    <option value="RU" selected="selected"><?php _e('Russia', 'fivepost-wp')?></option>
                                </select>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_region_code">
                                <?php _e('Region code', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Region code', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_region_code"
                                       id="woocommerce_fivepost_create_warehouse_region_code" />
                                <p class="description">
                                    <?php _e('Example', 'fivepost-wp').': 99'?>
                                </p>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_federal_district">
                                <?php _e('Federal district', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Federal district', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_federal_district"
                                       id="woocommerce_fivepost_create_warehouse_federal_district" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_region">
                                <?php _e('Region', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Region', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_region"
                                       id="woocommerce_fivepost_create_warehouse_region" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_index">
                                <?php _e('Index', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Index', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_index"
                                       id="woocommerce_fivepost_create_warehouse_index" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_city">
                                <?php _e('City', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('City', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_city"
                                       id="woocommerce_fivepost_create_warehouse_city" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_street">
                                <?php _e('Street', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Street', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_street"
                                       id="woocommerce_fivepost_create_warehouse_street" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_house_number">
                                <?php _e('House number', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('House number', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_house_number"
                                       id="woocommerce_fivepost_create_warehouse_house_number" />
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_coordinates_x">
                                <?php _e('Coordinate X', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Coordinate X', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_coordinates_x"
                                       id="woocommerce_fivepost_create_warehouse_coordinates_x" />
                                <p class="description">
                                    <?php _e('Example', 'fivepost-wp').': 55.652146'?>
                                </p>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_coordinates_y">
                                <?php _e('Coordinate Y', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Coordinate Y', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_coordinates_y"
                                       id="woocommerce_fivepost_create_warehouse_coordinates_y" />
                                <p class="description">
                                    <?php _e('Example', 'fivepost-wp').': 38.054088'?>
                                </p>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_contact_phone_number">
                                <?php _e('Contact phone number', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Contact phone number', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_contact_phone_number"
                                       id="woocommerce_fivepost_create_warehouse_contact_phone_number" />
                                <p class="description">
                                    <?php _e('Example', 'fivepost-wp').': +71111111111'?>
                                </p>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_time_zone">
                                <?php _e('Time zone', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Time zone', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_time_zone"
                                       id="woocommerce_fivepost_create_warehouse_time_zone" />
                                <p class="description">
                                    <?php _e('Example', 'fivepost-wp').': +02:00'?>
                                </p>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_working_time">
                                <?php _e('Working time', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Working time', 'fivepost-wp')?></span>
                                </legend>
                                <div>
                                    <?php foreach($days as $key => $day):?>
                                        <label for="woocommerce_fivepost_create_warehouse_working_time_from_<?php echo esc_attr($key)?>">
                                            <b><?php echo esc_html($day) ?>:</b>
                                        </label><br />
                                        <label for="woocommerce_fivepost_create_warehouse_working_time_from_<?php echo esc_attr($key) ?>">
                                            <?php _e('from', 'fivepost-wp')?>
                                        </label>
                                        <input class="input-text regular-input time-inputmask" type="text" style="width: 100px"
                                               name="woocommerce_fivepost_create_warehouse_working_time[<?php echo esc_attr($key)?>][from]"
                                               id="woocommerce_fivepost_create_warehouse_working_time_from_<?php echo esc_attr($key)?>" />

                                        <label for="woocommerce_fivepost_create_warehouse_working_time_to_<?php echo esc_attr($key)?>">
                                            <?php _e('to', 'fivepost-wp')?>
                                        </label>
                                        <input class="input-text regular-input time-inputmask" type="text" style="width: 100px"
                                               name="woocommerce_fivepost_create_warehouse_working_time[<?php echo esc_attr($key)?>][to]"
                                               id="woocommerce_fivepost_create_warehouse_working_time_to_<?php echo esc_attr($key)?>" /><br />

                                        <p class="description">
                                            <?php _e('Example', 'fivepost-wp').': 23:59'?>
                                        </p>
                                    <?php endforeach;?>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_create_warehouse_partner_location_id">
                                <?php _e('Partner location id', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Partner location id', 'fivepost-wp')?></span>
                                </legend>
                                <input class="input-text regular-input " type="text"
                                       name="woocommerce_fivepost_create_warehouse_partner_location_id"
                                       id="woocommerce_fivepost_create_warehouse_partner_location_id" />
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <button class="button-primary" type="button" id="create-warehouse">
                    <?php _e('Create', 'fivepost-wp')?>
                </button>
            </div>
            <div id="import-warehouses-wrapper" style="display: none">
                <table class="form-table">
                    <tr>
                        <th scope="row" class="titledesc">
                            <label for="woocommerce_fivepost_import_warehouses">
                                <?php _e('Choose file', 'fivepost-wp')?>
                            </label>
                        </th>
                        <td class="forminp">
                            <fieldset>
                                <legend class="screen-reader-text">
                                    <span><?php _e('Choose file with warehouses data in json format')?></span>
                                </legend>
                                <input type="file" name="woocommerce_fivepost_import_warehouses_file"
                                       id="woocommerce_fivepost_import_warehouses" />
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <button class="button-primary" type="button" id="import-warehouses">
                    <?php _e('Import', 'fivepost-wp')?>
                </button>
            </div>
        </form>

<?php $response['html'] = ob_get_clean();

        wp_send_json($response);
        wp_die();
    }

    public function actionWpAjaxSetWarehouseDefault() {
        $response = [
            'error' => '',
            'success' => ''
        ];

        if(!empty($_POST['warehouseId']) && is_numeric($_POST['warehouseId'])) {
            global $wpdb;

            $warehouse_id = intval($_POST['warehouseId']);
            $query = $wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}fivepost_warehouses` WHERE warehouse_id = %d", $warehouse_id);

            if($warehouse = $wpdb->get_row($query)) {
                $wpdb->query("UPDATE `{$wpdb->base_prefix}fivepost_warehouses` SET `is_default` = 0");
                $query = $wpdb->prepare(
                    "UPDATE `{$wpdb->base_prefix}fivepost_warehouses` SET `is_default` = 1 WHERE warehouse_id = %d",
                    $warehouse->warehouse_id
                );
                $wpdb->query($query);

                $response['success'] = 'Warehouse will be set by default after page refresh.';
            } else
                $response['error'] = 'Can\'t find warehouse.';
        } else
            $response['error'] = 'Failed to select warehouse.';

        wp_send_json($response);
        wp_die();
    }

    public function actionWpAjaxRemoveWarehouse() {
        $response = [
            'error' => '',
            'success' => ''
        ];

        if(!empty($_POST['warehouseId']) && is_numeric($_POST['warehouseId'])) {
            global $wpdb;

            $warehouse_id = intval($_POST['warehouseId']);
            $query = $wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}fivepost_warehouses` WHERE warehouse_id = %d", $warehouse_id);

            if($warehouse = $wpdb->get_row($query)) {
                if(!intval($warehouse->is_default)) {
                    $query = $wpdb->prepare(
                        "DELETE FROM `{$wpdb->base_prefix}fivepost_warehouses` WHERE warehouse_id = %d",
                        $warehouse->warehouse_id
                    );
                    $wpdb->query($query);

                    $response['success'] = 'Warehouse will be remove after page refresh.';
                } else
                    $response['error'] = 'Can\'t remove default warehouse.';
            } else
                $response['error'] = 'Can\'t find warehouse.';
        } else
            $response['error'] = 'Failed to select warehouse.';

        wp_send_json($response);
        wp_die();
    }

    public function actionWpAjaxCreateWarehouse() {
        $response = [
            'errors'   => [],
            'warnings' => [],
            'success'  => []
        ];
        $working_time = [];

        if(empty($_POST['woocommerce_fivepost_create_warehouse_name']))
            $response['errors'][] = __('Warehouse name is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_region_code']))
            $response['errors'][] = __('Warehouse region code is empty.', 'fivepost-wp');
        elseif(!is_numeric($_POST['woocommerce_fivepost_create_warehouse_region_code']))
            $response['errors'][] = __('Warehouse region code must be an number.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_federal_district']))
            $response['errors'][] = __('Warehouse federal district is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_region']))
            $response['errors'][] = __('Warehouse region is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_index']))
            $response['errors'][] = __('Warehouse index is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_city']))
            $response['errors'][] = __('Warehouse city is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_street']))
            $response['errors'][] = __('Warehouse street is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_house_number']))
            $response['errors'][] = __('Warehouse house number is empty.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_coordinates_x']) ||
           empty($_POST['woocommerce_fivepost_create_warehouse_coordinates_y']))
            $response['errors'][] = __('Warehouse coordinates is empty.', 'fivepost-wp');
        else {
            $_POST['woocommerce_fivepost_create_warehouse_coordinates_x'] = strtr(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_coordinates_x']), [
                ',' => '.'
            ]);
            $_POST['woocommerce_fivepost_create_warehouse_coordinates_y'] = strtr(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_coordinates_y']), [
                ',' => '.'
            ]);

            if(!is_numeric($_POST['woocommerce_fivepost_create_warehouse_coordinates_x']) ||
                !is_numeric($_POST['woocommerce_fivepost_create_warehouse_coordinates_y']))
                $response['errors'][] = __('Warehouse coordinates must be an number.', 'fivepost-wp');
        }

        if(empty($_POST['woocommerce_fivepost_create_warehouse_contact_phone_number']))
            $response['errors'][] = __('Warehouse contact phone number is empty.', 'fivepost-wp');
        elseif(!preg_match('/^\+7[0-9]{10}$/', $_POST['woocommerce_fivepost_create_warehouse_contact_phone_number'], $result))
            $response['errors'][] = __('Warehouse contact phone number is invalid.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_time_zone']))
            $response['errors'][] = __('Warehouse time zone is empty.', 'fivepost-wp');
        elseif(!preg_match('/^[+-][01][0-9]:00$/', $_POST['woocommerce_fivepost_create_warehouse_time_zone'], $result))
            $response['errors'][] = __('Warehouse time zone is invalid.', 'fivepost-wp');

        if(empty($_POST['woocommerce_fivepost_create_warehouse_working_time']) ||
           !is_array($_POST['woocommerce_fivepost_create_warehouse_working_time']))
            $response['errors'][] = __('Warehouse working time is empty.', 'fivepost-wp');
        else {
            foreach($_POST['woocommerce_fivepost_create_warehouse_working_time'] as $number => $day) {
                if(!is_numeric($number) || ($number < 1) || ($number > 7) || empty($day['from']) || empty($day['to']))
                    continue;

                if(!preg_match('/^([01][0-9]|2[0-3]):[0-5][0-9]$/', $day['from'], $result)) {
                    $response['errors'][] = sprintf(__('Warehouse time zone for row %d is invalid.', 'fivepost-wp'), $number);
                    continue;
                }

                if(!preg_match('/^([01][0-9]|2[0-3]):[0-5][0-9]$/', $day['to'], $result)) {
                    $response['errors'][] = sprintf(__('Warehouse time zone for row %d is invalid.', 'fivepost-wp'), $number);
                    continue;
                }

                $working_time[] = [
                    'dayNumber' => intval($number),
                    'timeFrom' => sanitize_text_field($day['from'].':00'),
                    'timeTill' => sanitize_text_field($day['to'].':00')
                ];
            }

            if(empty($working_time))
                $response['errors'][] = __('Warehouse working time is empty.', 'fivepost-wp');
        }

        if(empty($_POST['woocommerce_fivepost_create_warehouse_partner_location_id']))
            $response['errors'][] = __('Warehouse partner location id is empty.', 'fivepost-wp');

        if(!empty($response['errors']))
            $response['errors'] = '<div class="error inline create-warehouse-message"><p><strong>'.
                implode('</strong></p></div><div class="error inline create-warehouse-message"><p><strong>', $response['errors']).
                '</strong></p></div>';
        else {
            require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

            $shipping_method = new \Fivepost_Shipping_Method();
            $adapter = new CurlAdapter();
            $sdk     = new Sdk($adapter, '', null, $shipping_method->get_option('api_wtype'));
            $request = new JwtGenerate();
            $request->setApikey($shipping_method->get_option('api_key'));

            $jwt = $sdk->jwtGenerate($request)->getResponse();

            $adapter = new CurlAdapter();
            $sdk     = new Sdk($adapter, $jwt->getJwt(), null, $shipping_method->get_option('api_wtype'));

            $working_time_list = new WorkingTimeList();

            foreach($working_time as $day) {
                $working_time_day = new WorkingTime();
                $working_time_day->setDayNumber($day['dayNumber'])
                    ->setTimeFrom($day['timeFrom'])
                    ->setTimeTill($day['timeTill']);
                $working_time_list->add($working_time_day);
            }

            $warehouse_elem_list = new WarehouseElemList();
            $warehouse_elem = new WarehouseElem();

            $warehouse_elem->setName(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_name']))
                ->setCountryId(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_country_id']))
                ->setRegionCode(intval($_POST['woocommerce_fivepost_create_warehouse_region_code']))
                ->setFederalDistrict(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_federal_district']))
                ->setRegion(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_region']))
                ->setIndex(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_index']))
                ->setCity(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_city']))
                ->setStreet(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_street']))
                ->setHouseNumber(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_house_number']))
                ->setCoordinates(
                    floatval($_POST['woocommerce_fivepost_create_warehouse_coordinates_x']).', '.
                    floatval($_POST['woocommerce_fivepost_create_warehouse_coordinates_y'])
                )
                ->setContactPhoneNumber(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_contact_phone_number']))
                ->setTimeZone(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_time_zone']))
                ->setWorkingTime($working_time_list)
                ->setPartnerLocationId(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_partner_location_id']));
            $warehouse_elem_list->add($warehouse_elem);

            $request = new Warehouse();
            $request->setWarehouses($warehouse_elem_list);
            $result = $sdk->warehouse($request)->getResponse();
            if($result->getSuccess()) {
                $warehouses = $result->getWarehouses()->getFields();
                if(!empty($warehouses[0]) && !empty($warehouses[0]['id'])) {

                    if ($warehouses[0]['status']=='FAILED') {
                        $response['errors'][] = $warehouses[0]['description'];
                    } else {
                        global $wpdb;

                        $warehouse = [
                            'NULL',
                            "'".addslashes(sanitize_text_field($warehouses[0]['id']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_name']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_country_id']))."'",
                            intval($_POST['woocommerce_fivepost_create_warehouse_region_code']),
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_federal_district']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_region']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_index']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_city']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_street']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_house_number']))."'",
                            floatval($_POST['woocommerce_fivepost_create_warehouse_coordinates_x']),
                            floatval($_POST['woocommerce_fivepost_create_warehouse_coordinates_y']),
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_contact_phone_number']))."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_time_zone']))."'",
                            "'".json_encode($working_time)."'",
                            "'".addslashes(sanitize_text_field($_POST['woocommerce_fivepost_create_warehouse_partner_location_id']))."'",
                            '\'\''
                        ];

                        if($wpdb->query("REPLACE INTO `{$wpdb->base_prefix}fivepost_warehouses` VALUES (" . implode(', ', $warehouse) . ')'))
                            $response['success'][] = __('Warehouse has been created and will appear after the page is refreshed', 'fivepost-wp');
                        else
                            $response['errors'][] = __('Warehouse was created, but it could not be added to the site', 'fivepost-wp');
                    }
                } else
                    $response['errors'][] = __('Warehouse has been created but an unpredictable response has been received', 'fivepost-wp');
            } else {
                if(method_exists($result, 'getWarehouses')) {
                    $warehouses = $result->getWarehouses()->getFields();

                    if(!empty($warehouses[0]) && !empty($warehouses[0]['description']))
                        $response['errors'][] = $warehouses[0]['description'];
                    else
                        $response['errors'][] = __('Something went wrong. Try to repeat request.', 'fivepost-wp');
                } else
                    $response['errors'][] = $result->getMessage();
            }

            if(!empty($response['errors']))
                $response['errors'] = '<div class="error inline create-warehouse-message"><p><strong>'.
                    implode('</strong></p></div><div class="error inline create-warehouse-message"><p><strong>', $response['errors']).
                    '</strong></p></div>';
        }

        if(!empty($response['warnings']))
            $response['warnings'] = '<div class="notice notice-warning inline create-warehouse-message"><p><strong>'.
                implode('</strong></p></div><div class="notice notice-warning inline create-warehouse-message"><p><strong>', $response['warnings']).
                '</strong></p></div>';

        if(!empty($response['success']))
            $response['success'] = '<div class="updated inline create-warehouse-message"><p><strong>'.
                implode('</strong></p></div><div class="updated inline create-warehouse-message"><p><strong>', $response['success']).
                '</strong></p></div>';

        wp_send_json($response);
        wp_die();
    }

    public function actionWpAjaxImportWarehouses() {
        $response = [
            'errors'   => [],
            'warnings' => [],
            'success'  => []
        ];
        $values = [];

        if(!empty($_FILES['woocommerce_fivepost_import_warehouses_file']) &&
           !empty($_FILES['woocommerce_fivepost_import_warehouses_file']['tmp_name'])) {
            if($file_content = @file_get_contents($_FILES['woocommerce_fivepost_import_warehouses_file']['tmp_name'])) {
                if(($file_json = @json_decode($file_content)) && is_array($file_json)) {
                    foreach($file_json as $key => $warehouse) {
                        $warehouse_errors = [];

                        if(empty($warehouse->IPOL_FIVEPOST_WH_uuid))
                            $response['warnings'][] = sprintf(__('Row %d: Warehouse uuid is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_name))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse name is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_countryId))
                            $response['warnings'][] = sprintf(__('Row %d: Warehouse country ID is empty. The parameter will be set with the value "RU".', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_regionCode))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse region code is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_federalDistrict))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse federal district is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_region))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse region is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_index))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse index is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_city))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse city is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_street))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse street is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_houseNumber))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse house number is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_coordinatesX) || empty($warehouse->IPOL_FIVEPOST_WH_coordinatesY))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse coordinates is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_contactPhoneNumber))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse contact phone number is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_timeZone))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse time zone is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_workingTime))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse working time is empty.', 'fivepost-wp'), $key + 1);
                        if(empty($warehouse->IPOL_FIVEPOST_WH_partnerLocationId))
                            $warehouse_errors[] = sprintf(__('Row %d: Warehouse partner location id is empty.', 'fivepost-wp'), $key + 1);

                        if(!empty($warehouse_errors)) {
                            $response['errors'] = array_merge($response['errors'], $warehouse_errors);
                            continue;
                        }

                        $response['success'][] = sprintf(__('Warehouse "%s" was added.', 'fivepost-wp'), $warehouse->IPOL_FIVEPOST_WH_name);

                        $values[] = implode(', ', [
                            'NULL',
                            !empty($warehouse->IPOL_FIVEPOST_WH_uuid) ? "'".addslashes($warehouse->IPOL_FIVEPOST_WH_uuid)."'" : 'NULL',
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_name)."'",
                            !empty($warehouse->IPOL_FIVEPOST_WH_countryId) ? "'".addslashes($warehouse->IPOL_FIVEPOST_WH_countryId)."'" : 'NULL',
                            intval($warehouse->IPOL_FIVEPOST_WH_regionCode),
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_federalDistrict)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_region)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_index)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_city)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_street)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_houseNumber)."'",
                            floatval($warehouse->IPOL_FIVEPOST_WH_coordinatesX),
                            floatval($warehouse->IPOL_FIVEPOST_WH_coordinatesY),
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_contactPhoneNumber)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_timeZone)."'",
                            "'".json_encode($warehouse->IPOL_FIVEPOST_WH_workingTime)."'",
                            "'".addslashes($warehouse->IPOL_FIVEPOST_WH_partnerLocationId)."'",
                            'NULL'
                        ]);
                    }
                } else
                    $response['errors'][] = __('Failed to read file. Make sure the file with the correct settings and in correct format.', 'fivepost-wp');
            } else
                $response['errors'][] = __('Failed to read file. Make sure the file with the correct settings and in correct format.', 'fivepost-wp');
        } else
            $response['errors'][] = __('Failed to read file. Make sure the file with the correct settings and in correct format.', 'fivepost-wp');

        if(!empty($values)) {
            global $wpdb;

            $wpdb->query("REPLACE INTO `{$wpdb->base_prefix}fivepost_warehouses` VALUES (" . implode('),(', $values) . ')');
        }

        if(!empty($response['errors']))
            $response['errors'] = '<div class="error inline warehouse-message"><p><strong>'.
                implode('</strong></p></div><div class="error inline warehouse-message"><p><strong>', $response['errors']).
                '</strong></p></div>';

        if(!empty($response['warnings']))
            $response['warnings'] = '<div class="notice notice-warning inline warehouse-message"><p><strong>'.
                implode('</strong></p></div><div class="notice notice-warning inline warehouse-message"><p><strong>', $response['warnings']).
                '</strong></p></div>';

        if(!empty($response['success']))
            $response['success'] = '<div class="updated inline warehouse-message"><p><strong>'.
                implode('</strong></p></div><div class="updated inline warehouse-message"><p><strong>', $response['success']).
                '</strong></p></div>';

        wp_send_json($response);
        wp_die();
    }

    public function actionWpAjaxExportWarehouses() {
        global $wpdb;

        $warehouses = $wpdb->get_results("SELECT * FROM `{$wpdb->base_prefix}fivepost_warehouses`");
        $json = [];
        $upload_dir = wp_get_upload_dir();
        $file_path = $upload_dir['basedir'].'/5post-warehouses.json';
        $file_url = $upload_dir['baseurl'].'/5post-warehouses.json';

        foreach($warehouses as $warehouse) {
            $row = [
                'IPOL_FIVEPOST_WH_name' => $warehouse->name,
                'IPOL_FIVEPOST_WH_regionCode' => $warehouse->region_code,
                'IPOL_FIVEPOST_WH_federalDistrict' => $warehouse->federal_district,
                'IPOL_FIVEPOST_WH_region' => $warehouse->region,
                'IPOL_FIVEPOST_WH_index' => $warehouse->index,
                'IPOL_FIVEPOST_WH_city' => $warehouse->city,
                'IPOL_FIVEPOST_WH_street' => $warehouse->street,
                'IPOL_FIVEPOST_WH_houseNumber' => $warehouse->house_number,
                'IPOL_FIVEPOST_WH_coordinatesX' => $warehouse->coordinates_x,
                'IPOL_FIVEPOST_WH_coordinatesY' => $warehouse->coordinates_y,
                'IPOL_FIVEPOST_WH_contactPhoneNumber' => $warehouse->contact_phone_number,
                'IPOL_FIVEPOST_WH_timeZone' => $warehouse->time_zone,
                'IPOL_FIVEPOST_WH_workingTime' => json_decode($warehouse->working_time),
                'IPOL_FIVEPOST_WH_partnerLocationId' => $warehouse->partner_location_id
            ];

            if(!empty($warehouse->uuid))
                $row['IPOL_FIVEPOST_WH_uuid'] = $warehouse->uuid;

            if(!empty($warehouse->country_id))
                $row['IPOL_FIVEPOST_WH_countryId'] = $warehouse->country_id;

            $json[] = $row;
        }

        if(file_exists($file_path))
            unlink($file_path);

        file_put_contents($file_path, json_encode($json));

        wp_send_json([
            'url' => $file_url
        ]);
        wp_die();
    }

    /**
     * Adding MetaBox 5P Shipping in ADMIN - edit order page
    */
    public function actionAddMetaBoxes() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

        $fivepost_shipping_method = new \Fivepost_Shipping_Method();
        $order = wc_get_order();
        $shipping_methods = [];

        if ($order) {
            foreach($order->get_shipping_methods() as $shipping_method) $shipping_methods[] = $shipping_method->get_method_id();
        }

        if ($fivepost_shipping_method->has_settings() && $fivepost_shipping_method->get_option('api_key'))
        if(!in_array('fivepost_shipping_method', $shipping_methods)) {
            if (isset($fivepost_shipping_method->settings['always_show_winbox']))
                if ($fivepost_shipping_method->settings['always_show_winbox']=='always')
                    add_meta_box('fivepost-wp' . '-panel', $fivepost_shipping_method->get_method_title(), [$this, 'callbackMetaBoxesEmpty'], 'shop_order', 'side', 'high');
        } else add_meta_box('fivepost-wp' . '-panel', $fivepost_shipping_method->get_method_title(), [$this, 'callbackMetaBoxes'], 'shop_order', 'side', 'high');
    }

    public function callbackMetaBoxesEmpty() {
        $order = wc_get_order();
        ?>
        <div class="order-info-wrapper">
            <button type="button" data-order="<?php echo esc_attr($order->get_id())?>" class="button button-primary select_fivepost_for_neworder"><?php __('Select this delivery service','fivepost-wp')?></button>
        </div>
        <?php
    }
    /**
     * AJAX Handler - select THIS delivery system (empty!)
     */
    public function actionWpAjaxFivepostSelectService() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');
        $shipping_method = new \Fivepost_Shipping_Method();
        if (isset($_POST['order_id'])) {
            if($order = wc_get_order(wc_sanitize_order_id($_POST['order_id']))) {
                foreach ($order->get_items('shipping') as $shipping_item) {
                    /**
                     * @var \WC_Order_Item_Shipping $shipping_item
                     */
                    $order->remove_item($shipping_item->get_id());
                }

                $rate = new \WC_Shipping_Rate();
                $rate->set_id(sanitize_text_field(\Fivepost_Shipping_Method::SHIPPING_METHOD_ID.':pickup'));
                $rate->set_cost(0);
                $rate->set_label($shipping_method->get_title());

                $new_shipping_item = new \WC_Order_Item_Shipping;
                $new_shipping_item->set_shipping_rate($rate);
                $new_shipping_item->set_method_id(\Fivepost_Shipping_Method::SHIPPING_METHOD_ID);
                $order->add_item($new_shipping_item);
                $order->calculate_totals();
                wp_send_json([
                    'type' => 'created'
                ]);
                wp_die();
            }
        }
        wp_send_json([
            'type' => 'error',
            'message' => __('An error has occurred. Please try later.', 'fivepost-wp')
        ]);
        wp_die();
    }

    /**
     * MetaBox Content
     */
    public function callbackMetaBoxes() {
        global $wpdb;
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');

        $shipping_method = new \Fivepost_Shipping_Method();
        $en_nopoint = true;
        if ($shipping_method->settings['enshp_force']!='yes') $en_nopoint = false;
        $no_point = false;

        $order = wc_get_order();
        $data = [
            'id'         => $order->get_id(),
            'first_name' => $order->get_shipping_first_name() ? : $order->get_billing_first_name(),
            'last_name'  => $order->get_shipping_last_name() ? : $order->get_billing_last_name(),
            'phone'      => $order->get_billing_phone(),
            'email'      => $order->get_billing_email(),
            'package' => [
                'weight' => 0,
                'length' => 0,
                'width'  => 0,
                'height' => 0
            ],
            'shipping' => [],
            'warehouses'=>[],
            'fivepost' => [],
            'full_paid' => in_array($order->get_payment_method(), (array) $shipping_method->get_option('online_payments'))
        ];

        if(($fivepost_order_id = $order->get_meta('_fivepost_order_id')) && ($fivepost_sender_order_id = $order->get_meta('_fivepost_sender_order_id'))) {
            $data['fivepost']['order_id'] = $fivepost_order_id;
            $data['fivepost']['sender_order_id'] = $fivepost_sender_order_id;

            $adapter = new CurlAdapter();
            $sdk = new Sdk($adapter, '', null, $shipping_method->get_option('api_wtype'));
            $request = new JwtGenerate();
            $request->setApikey($shipping_method->get_option('api_key'));

            $jwt = $sdk->jwtGenerate($request)->getResponse();
            $adapter = new CurlAdapter();
            $sdk = new Sdk($adapter, $jwt->getJwt(), null, $shipping_method->get_option('api_wtype'));
            $get_order_history = new GetOrderHistory();
            $get_order_history->setOrderId($fivepost_order_id);
            $response = $sdk->getOrderHistory($get_order_history)->getResponse();

            if(method_exists($response, 'getHistory') && is_array($response->getHistory()->getFields()))
                $data['fivepost']['history'] = $response->getHistory()->getFields();

            $data['fivepost']['printlabel'] = false;
            $data['fivepost']['printlabel'] = $order->get_meta('_fivepost_order_id_labelprintfile');
            if ($data['fivepost']['printlabel'])
                if (!file_exists(FIVEPOST_PLUGIN_DIR.'files/'.$data['fivepost']['printlabel'])) $data['fivepost']['printlabel']=false;

        }
        $point_id = $order->get_meta('_fivepost_point_id');
        $data['shipping']['emptypoint']=false;
        if (!$point_id || ($point_id=='NoPoint') || ($point_id=='') ) {
            $data['shipping']['emptypoint']=true;
        }


        $max_length = $max_width = $max_height = 0;
        $quantity = 0;

        $cargogg = new \Ipol\Fivepost\Core\Delivery\Cargo();
        foreach($order->get_items() as $item) {
            /**
             * @var \WC_Order_Item_Product $item
             */
            $dimensions = $this->_getItemDimensions($item, $shipping_method);

            $cargoitm = new CargoItem();
            $cargoitm->setHeight($dimensions['height'])
            ->setWidth($dimensions['width'])
            ->setLength($dimensions['length'])
            ->setWeight($dimensions['weight'])
            ->setQuantity($item->get_quantity());
            $cargogg->add($cargoitm);

        }
        $gabs = $cargogg->getGabs();
        $data['package']['length']=$gabs['G']['L']/10;
        $data['package']['width']=$gabs['G']['W']/10;
        $data['package']['height']=$gabs['G']['H']/10;
        $data['package']['weight']=$gabs['W']/1000;

        foreach($order->get_items('shipping') as $shipping_item) {
            /**
             * @var \WC_Order_Item_Shipping $shipping_item
             */
            if($shipping_item->get_method_id() == \Fivepost_Shipping_Method::SHIPPING_METHOD_ID) {
                $data['shipping']['title'] = $shipping_item->get_method_title();
                break;
            }
        }

        $paysystem = $order->get_payment_method();
        $is_paycash=false;
        $is_paycard=false;
        if (is_array($shipping_method->settings['npp_payments'])) $is_paycash = in_array($paysystem,$shipping_method->settings['npp_payments']);
        if (is_array($shipping_method->settings['online_payments'])) $is_paycard = in_array($paysystem,$shipping_method->settings['online_payments']);
        $data['pay_method']=0;//prepayment
        if ($is_paycash) {
            $data['pay_method']=1;
        } elseif ($is_paycard) {
            $data['pay_method']=2;
        }

        $package = [
            'destination' => [
                'country' => $order->get_shipping_country() ? : $order->get_billing_country(),
                'city' => $order->get_shipping_city() ? : $order->get_billing_city()
            ],
            'contents' => []
        ];

        foreach($order->get_items() as $item) {
            /**
             * @var \WC_Order_Item_Product $item
            */

            $package['contents'][] = [
                'data' => $item->get_product(),
                'quantity' => $item->get_quantity()
            ];
        }

        $forsed=true;
        $var = intval($order->get_meta('_fivepost_order_forse_wei'));
        if ($var>0) $package['forse']['WEI']=$var; else $forsed=false;
        $var = intval($order->get_meta('_fivepost_order_forse_l'));
        if ($var>0) $package['forse']['L']=$var; else $forsed=false;
        $var = intval($order->get_meta('_fivepost_order_forse_w'));
        if ($var>0) $package['forse']['W']=$var; else $forsed=false;
        $var = intval($order->get_meta('_fivepost_order_forse_h'));
        if ($var>0) $package['forse']['H']=$var; else $forsed=false;
        if (isset($package['forse']) && !$forsed) unset($package['forse']);
        if (isset($package['forse'])) {
            $data['package']['weight']=$package['forse']['WEI']/1000;
            $data['package']['length']=$package['forse']['L'];
            $data['package']['width']=$package['forse']['W'];
            $data['package']['height']=$package['forse']['H'];
        }

        $data['shipping']['rates'] = $shipping_method->get_rates_for_package($package);
        $data['shipping']['addDays'] = intval($shipping_method->settings['add_dlvtime']);

        //Add All Warehouses
        $warehouses = $wpdb->get_results("SELECT * FROM `{$wpdb->base_prefix}fivepost_warehouses` ORDER BY `warehouse_id`;");
        foreach ($warehouses as $wh) {
            $data['warehouses'][]=[
                'id'=>$wh->warehouse_id,
                'name'=>$wh->name,
                'default'=>$wh->is_default
            ];
        }
        require_once FIVEPOST_PLUGIN_DIR.'/admin/meta-box.php';
    }
    /** AJAX Handler - saving data for force calc */
    public function actionWpAjaxFivepostSaveOrderCalcdata() {
        if (isset($_POST['order_id'])) {
            if($order = wc_get_order(wc_sanitize_order_id($_POST['order_id']))) {
                $updated=false;
                if (isset($_POST['WEI'])) {
                    $var = intval(floatval($_POST['WEI'])*1000);
                    $order->update_meta_data('_fivepost_order_forse_wei', $var);
                    $updated=true;
                }
                if (isset($_POST['L'])) {
                    $var = intval(floatval($_POST['L']));
                    $order->update_meta_data('_fivepost_order_forse_l', $var);
                    $updated=true;
                }
                if (isset($_POST['W'])) {
                    $var = intval(floatval($_POST['W']));
                    $order->update_meta_data('_fivepost_order_forse_w', $var);
                    $updated=true;
                }
                if (isset($_POST['H'])) {
                    $var = intval(floatval($_POST['H']));
                    $order->update_meta_data('_fivepost_order_forse_h', $var);
                    $updated=true;
                }
                if ($updated) $order->save();
                wp_send_json([
                    'type' => 'success'
                ]);
                wp_die();
            }
        }
        wp_send_json([
            'type' => 'error',
            'message' => __('Unknown error when saving order data', 'fivepost-wp')
        ]);
        wp_die();
    }


    /**
     * AJAX Handler - changing shipping method
     */
    public function actionWpAjaxFivepostUpdateShipping() {
        require_once(FIVEPOST_PLUGIN_DIR.'classes/shipping.class.php');
        $shipping_method = new \Fivepost_Shipping_Method();
        if(isset($_POST['order_id']) && !empty($_POST['point']) && !empty($_POST['point']['id']) && !empty($_POST['point']['cost'])) {
            if($order = wc_get_order(wc_sanitize_order_id($_POST['order_id']))) {
                $order->update_meta_data('_fivepost_point_id', sanitize_text_field($_POST['point']['id']));

                foreach ($order->get_items('shipping') as $shipping_item) {
                    /**
                     * @var \WC_Order_Item_Shipping $shipping_item
                     */
                    $order->remove_item($shipping_item->get_id());
                }

                $rate = new \WC_Shipping_Rate();
                $rate->set_id(sanitize_text_field(\Fivepost_Shipping_Method::SHIPPING_METHOD_ID.':pickup'));
                $rate->set_cost((int)sanitize_text_field($_POST['point']['cost']));
                $rate->set_label($shipping_method->get_title().' ('.sanitize_text_field($_POST['point']['name']).')');

                $new_shipping_item = new \WC_Order_Item_Shipping;
                $new_shipping_item->set_shipping_rate($rate);
                $new_shipping_item->set_method_id(\Fivepost_Shipping_Method::SHIPPING_METHOD_ID);
                $order->add_item($new_shipping_item);
                $order->calculate_totals();

                wp_send_json([
                    'type' => 'success',
                    'message' => __('Data saved successfully', 'fivepost-wp')
                ]);
                wp_die();
            }
        }

        wp_send_json([
            'type' => 'error',
            'message' => __('You must select a point on the map', 'fivepost-wp')
        ]);
        wp_die();
    }

    /**
     * AJAX Handler - Send Order
     */
    public function actionWpAjaxFivepostSendOrder() {
        global $wpdb;

        if(!empty($_POST['form']) && !empty($_POST['form']['order_id'])) {
            $form = [
                'order_id'=>wc_sanitize_order_id($_POST['form']['order_id']),
                'first_name'=>sanitize_text_field($_POST['form']['first_name']),
                'last_name'=>sanitize_text_field($_POST['form']['last_name']),
                'phone'=>sanitize_text_field($_POST['form']['phone']),
                'email'=>sanitize_email($_POST['form']['email']),
                'type'=>sanitize_text_field($_POST['form']['email']),
                'legal_name'=>sanitize_text_field($_POST['form']['legal_name']),
                'package_weight'=>sanitize_text_field($_POST['form']['package_weight']),
                'package_length'=>sanitize_text_field($_POST['form']['package_length']),
                'package_width'=>sanitize_text_field($_POST['form']['package_width']),
                'package_height'=>sanitize_text_field($_POST['form']['package_height']),
                'from_place'=>sanitize_text_field($_POST['form']['from_place']),
                'ptype'=>sanitize_text_field($_POST['form']['ptype']),
                'wh'=>intval($_POST['form']['wh'])
            ]; //$_POST['form'];

            if($wc_order = wc_get_order(wc_sanitize_order_id($form['order_id']))) {
                $fivepost_pointid = $wc_order->get_meta('_fivepost_point_id');
                if (!$fivepost_pointid || ($fivepost_pointid=='NoPoint')) {
                    wp_send_json([
                        'type' => 'error',
                        'message' => __('You must select a pickup point for delivery','fivepost-wp')
                    ]);
                    wp_die();
                }

                require_once(FIVEPOST_PLUGIN_DIR . 'classes/shipping.class.php');

                $shipping_method = new \Fivepost_Shipping_Method();

                if($warehouse = $wpdb->get_row(
                    $wpdb->prepare("SELECT * FROM `{$wpdb->base_prefix}fivepost_warehouses` WHERE `warehouse_id` = %d;",$form['wh'])
                )) {
                    if ($this->_validateSendForm($form)) {

                        $delivery_cost = 0;
                        $payment_value = floatval($wc_order->get_total());
                        $pay_type='Bill';

                        $cargogg = new \Ipol\Fivepost\Core\Delivery\Cargo();
                        $itmCl = new ItemCollection();
                        $tax = new \WC_Tax();
                        foreach($wc_order->get_items() as $wc_item) {
                            /**
                             * @var \WC_Order_Item_Product $wc_item
                             */
                            $gbs = $this->_getItemDimensions($wc_item,$shipping_method);
                            $price = new Money($wc_item->get_product()->get_price());

                            $cargoitm = new CargoItem(); //new \Ipol\Fivepost\Core\Delivery\Cargo();
                            $cargoitm->setHeight($gbs['height'])
                                ->setWidth($gbs['width'])
                                ->setLength($gbs['length'])
                                ->setPrice($price)
                                ->setCost($price)
                                ->setWeight($gbs['weight'])
                                ->setQuantity($wc_item->get_quantity());
                            $cargogg->add($cargoitm);

                            $tx = $tax->get_base_tax_rates($wc_item->get_product()->get_tax_class()); // 0,10,20,-1(=NOTax)
                            $txrate=-1; //first fit tax_rate
                            foreach ($tx as $t) if (in_array($t['rate'],[0,10,20])) {
                                $txrate=intval($t['rate']);
                                break;
                            }
                            $item = new Item();
                            $item->setName($wc_item->get_product()->get_name())
                                ->setPrice($price)
                                ->setQuantity($wc_item->get_quantity())
                                ->setId($wc_item->get_product()->get_id())
                                ->setWeight(intval($wc_item->get_product()->get_weight()))
                                ->setHeight(intval($wc_item->get_product()->get_height()))
                                ->setWidth(intval($wc_item->get_product()->get_width()))
                                ->setLength(intval($wc_item->get_product()->get_length()))
                                ->setVatRate($txrate)
                                ->setArticul($wc_item->get_product()->get_sku());
                            $itmCl->add($item);
                        }
                        foreach($wc_order->get_items('shipping') as $shipping_item) {
                            /**
                             * @var \WC_Order_Item_Shipping $shipping_item
                             */
                            if($shipping_item->get_method_id() == \Fivepost_Shipping_Method::SHIPPING_METHOD_ID) {
                                $delivery_cost = floatval($shipping_item->get_total());
                                $delivery_cost+=floatval($shipping_item->get_total_tax());
                                break;
                            }
                        }

                        $is_beznal=true;
                        if ($form['ptype']!='0') {
                            $payment_value = 0;
                            if ($form['ptype']=='1') $pay_type='Cash';
                            if ($form['ptype']=='2') $pay_type='Card';
                            $is_beznal=false;
                        }

                        $r = new Receiver();
                        $r->setEmail($form['email'])
                            ->setFirstName($form['first_name'])
                            ->setFullName($form['first_name'].' '.$form['last_name'])
                            ->setSecondName($form['last_name'])
                            ->setPhone($form['phone']);
                        $rCl = new ReceiverCollection();
                        $rCl->add($r);

                        $wp_adr = $wc_order->get_address();
                        $adr = new Address();
                        $adr->setCity($wp_adr['city'])
                        ->setRegion($wp_adr['state'])
                        ->setZip($wp_adr['postcode'])
                        ->setComment($wp_adr['address_1'].' '.$wp_adr['address_1'])
                        ->setCountry($wp_adr['country']);

                        $delivery_cost = new Money($delivery_cost);
                        $payment_value = new Money($payment_value);

                        $pay = new Payment();
                        $pay->setDelivery($delivery_cost)
                        ->setGoods($cargogg->getTotalPrice())
                        ->setPayed($payment_value)
                        ->setEstimated($cargogg->getTotalCost())
                        ->setIsBeznal($is_beznal)
                        ->setType($pay_type);//Cash | Card | Bill - page 35 manual

                        //Goods
                        $cargo_height = floatval(strtr($form['package_height'], [',' => '.'])) * 10; //mm
                        $cargo_length = floatval(strtr($form['package_length'], [',' => '.'])) * 10; //mm
                        $cargo_width = floatval(strtr($form['package_width'], [',' => '.'])) * 10;   //mm
                        $cargo_weight = floatval(strtr($form['package_weight'], [',' => '.'])) * 1000; //mg

                        $goods = new Goods();
                        $goods->setWeight(intval($cargo_weight))
                            ->setLength(intval($cargo_length))
                            ->setWidth(intval($cargo_width))
                            ->setHeight(intval($cargo_height))
                            ->setPositions(1);

                        $sender = new Sender();
                        //$sender->setCompany(get_bloginfo('name'));
                        if ($shipping_method->settings['sender_brand']!='') $sender->setCompany( $shipping_method->settings['sender_brand'] );

                        //$barcode = $this->_generateBarcode($shipping_method);
                        //$barcode = '12340000001027';

                        $returnopt='RETURN';
                        if ($shipping_method->settings['unclaimed_orders']=='mode2') $returnopt='UTILIZATION';

                        $ord = new Order();
                        $part_num=(string)$shipping_method->settings['partner_number'];
                        $ord_num=(string)$wc_order->get_order_number();
                        $zeroes='';
                        while ( (strlen($part_num)+strlen($ord_num)+strlen($zeroes))<14 ) $zeroes.='0';
                        $ord_num = $part_num.$zeroes.$ord_num;
                        $ord->setNumber($ord_num) //'WP_'.$wc_order->get_order_number()
                            ->setReceivers($rCl)
                            ->setAddressTo($adr)
                            ->setPayment($pay)
                            ->setGoods($goods)
                            ->setItems($itmCl)
                            ->setSender($sender)
                            ->setField('track',$ord_num) //'WP_'.$wc_order->get_order_number()
                            ->setField('senderLocation',$warehouse->partner_location_id)
                            ->setField('receiverLocation',$wc_order->get_meta('_fivepost_point_id'))
                            ->setField('undeliverableOption',$returnopt) // UTILIZATION | RETURN
                            ->setField('senderCargoId',$ord_num) //$barcode
                            //->setField('barcodes',$barcode)
                            ->setField('currency','RUB')
                        ;
                        //$ord->setStatus();

                        //LastCheck before send!
                        $point = $wpdb->get_row($wpdb->prepare("SELECT * from `{$wpdb->base_prefix}fivepost_pickup_points` WHERE `id` = %s;",$wc_order->get_meta('_fivepost_point_id') ));
                        if (!$point) {
                            wp_send_json([
                                'type' => 'error',
                                'message' => __('This point is currently unavailable','fivepost-wp')
                            ]);
                            wp_die();
                        }
                        //Check GABS and paysystem
                        $is_card = ($form['ptype']=='2');
                        $is_cash = ($form['ptype']=='1');
                        $check_pass = true;
                        $check_errors=[];
                        if ($is_cash && $point->cash_allowed=='0') {
                            $check_pass=false;
                            $check_errors[]=__('This point does not support cash payments','fivepost-wp');
                        }
                        if ($is_card && $point->card_allowed=='0') {
                            $check_pass=false;
                            $check_errors[]=__('This point does not support card payments','fivepost-wp');
                        }
                        if ($cargo_height > intval($point->max_cell_height)) {
                            $check_pass=false;
                            $check_errors[]=__('The point you have chosen cannot accept the cargo - the height is too high','fivepost-wp');
                        }
                        if ($cargo_length > intval($point->max_cell_length)) {
                            $check_pass=false;
                            $check_errors[]=__('The point you have chosen cannot accept the cargo - the length is too high','fivepost-wp');
                        }
                        if ($cargo_width > intval($point->max_cell_width)) {
                            $check_pass=false;
                            $check_errors[]=__('The point you have chosen cannot accept the cargo - the width is too high','fivepost-wp');
                        }
                        if (($cargo_weight*1000) > intval($point->max_weight)) {
                            $check_pass=false;
                            $check_errors[]=__('The point you have chosen cannot accept the cargo - the weight is too high','fivepost-wp');
                        }
                        if (!$check_pass) {
                            $errstr = __('The selected pick-up point cannot accept the goods','fivepost-wp')."\n\n";
                            foreach ($check_errors as $er) $errstr.=$er."\n";
                            wp_send_json([
                                'type' => 'error',
                                'message' => $errstr
                            ]);
                            wp_die();
                        }

                        $cntl = new MainController($shipping_method->get_option('api_key'),($shipping_method->get_option('api_wtype')=='TEST'));
                        $cntl->setOrder($ord);
                        $response = $cntl->Send();
                        if ($response->isSuccess()) {
                            $resp = $response->getData()->getResponse();
                            $cnt = $resp->getContentList();
                            $cnt = $cnt->getFirst();
                            if (!$cnt->isCreated()) {
                                //Error
                                $ermsg=__('Errors occurred while submitting the order:', 'fivepost-wp')."\n\n";
                                $ers = $cnt->getErrors();
                                while ($er = $ers->getNext()) {
                                    $ermsg.=$er->getCode().': '.$er->getMessage()."\n";
                                }
                                wp_send_json([
                                    'type' => 'error',
                                    'message' => $ermsg
                                ]);
                                wp_die();
                            } else {
                                $cargoes = $cnt->getCargoes();
                                $cargoes_ar=[];
                                while ($crg = $cargoes->getNext()) {
                                    $cargoes_ar[]=[
                                        'id'=>$crg->getCargoId(),
                                        'snd'=>$crg->getSenderCargoId(),
                                        'bc'=>$crg->getBarcode()
                                    ];
                                }
                                $wc_order->update_meta_data('_fivepost_order_id',  $cnt->getOrderId());
                                $wc_order->update_meta_data('_fivepost_sender_order_id',  $cnt->getSenderOrderId());
                                $wc_order->update_meta_data('_fivepost_sender_order_id_time',  date('Y-m-d H:i:s'));
                                $wc_order->update_meta_data('_fivepost_order_cargoes',  serialize($cargoes_ar));
                                $wc_order->update_meta_data('_fivepost_order_paytype',  $pay_type);
                                $wc_order->save();
                                wp_send_json([
                                    'type' => 'success',
                                    'message' => __('Order successfully created with number', 'fivepost-wp').' №'.$cnt->getSenderOrderId()
                                ]);
                                wp_die();
                            }
                        } else {
                            wp_send_json([
                                'type' => 'error',
                                'message' => $response->getErrorText()
                            ]);
                            wp_die();
                        }
                    }
                } else {
                    wp_send_json([
                        'type' => 'error',
                        'message' => __('The warehouse is not selected by default. Check plugin settings.', 'fivepost-wp')
                    ]);
                    wp_die();
                }
            }
        }
        wp_send_json([
            'type' => 'error',
            'message' => __('An error has occurred. Please try later.', 'fivepost-wp')
        ]);
        wp_die();
    }

    /**
     * AJAX Handler for Cancel Order
     */
    public function actionWpAjaxFivepostCancelOrder() {
        if(!empty($_POST['order_id'])) {
            if($wc_order = wc_get_order(wc_sanitize_order_id($_POST['order_id']))) {
                require_once(FIVEPOST_PLUGIN_DIR . 'classes/shipping.class.php');
                $fivepost_shipping_method = new \Fivepost_Shipping_Method();
                if ($fivepost_shipping_method->has_settings() && $fivepost_shipping_method->get_option('api_key')) {
                    if($order_id = $wc_order->get_meta('_fivepost_order_id')) {
                        $adapter = new CurlAdapter();
                        $sdk = new Sdk($adapter, '', null, $fivepost_shipping_method->get_option('api_wtype'));
                        $request = new JwtGenerate();
                        $request->setApikey($fivepost_shipping_method->get_option('api_key'));

                        $jwt = $sdk->jwtGenerate($request)->getResponse();
                        $adapter = new CurlAdapter();
                        $sdk = new Sdk($adapter, $jwt->getJwt(), null, $fivepost_shipping_method->get_option('api_wtype'));
                        $request = new CancelOrderById($order_id);
                        sleep(2);
                        $result = $sdk->CancelOrderById($request)->getResponse();

                        $delmes = $result->getDecoded();

                        if (!$delmes->error) {
                            wp_send_json([
                                'type' => 'success',
                                'message' => __('Order canceled successfully', 'fivepost-wp')
                            ]);
                            wp_die();
                        } else { //error
                            //errorMessage
                            wp_send_json([
                                'type' => 'error',
                                'message' => __('Error', 'fivepost-wp').': '.$delmes->errorMessage
                            ]);
                            wp_die();
                        }
                        exit;
                    }
                    exit;
                }
            }
        }

        wp_send_json([
            'type' => 'error',
            'message' => __('An error has occurred. Please try later.', 'fivepost-wp')
        ]);
        wp_die();
    }

    /**
     * AJAX Handler - Print Order Labels
     *
     * @return void
     */
    public function actionWpAjaxFivepostPrintLabel() {
        if(!empty($_POST['order_id'])) {
            if($wc_order = wc_get_order(wc_sanitize_order_id($_POST['order_id']))) {
                require_once(FIVEPOST_PLUGIN_DIR . 'classes/shipping.class.php');
                $fivepost_shipping_method = new \Fivepost_Shipping_Method();
                if ($fivepost_shipping_method->has_settings() && $fivepost_shipping_method->get_option('api_key')) {
                    if ( ($order_uid = $wc_order->get_meta('_fivepost_order_id')) && ($order_time = $wc_order->get_meta('_fivepost_sender_order_id_time')) ) {
                        $seconds = time() - strtotime($order_time);
                        if ($seconds<70) { //Not enough time !!!
                            wp_send_json([
                                'type' => 'error',
                                'message' => __("Must be at least 60 seconds after order creation.", 'fivepost-wp')."\n".__("Please, try again later.", 'fivepost-wp')
                            ]);
                            wp_die();
                        }

                        $cntl = new MainController($fivepost_shipping_method->get_option('api_key'), ($fivepost_shipping_method->get_option('api_wtype') == 'TEST'));
                        $res = $cntl->printLabels($order_uid);
                        //$res = $cntl->printLabels('b4ac0bb7-9197-4141-b82e-e8d541910be2');

                        if ($res->isSuccess()) {
                            $wc_order->update_meta_data('_fivepost_order_id_labelprintfile', $res->getData());
                            $wc_order->save();
                            wp_send_json([
                                'type' => 'success',
                                 'message' => __("PrintLabel received successfully", 'fivepost-wp'),
                                'fname'=>FIVEPOST_PLUGIN_URI.'files/'.$res->getData()
                            ]);
                            wp_die();
                        } else {
                            wp_send_json([
                                'type' => 'error',
                                'message' => __("An error has occurred", 'fivepost-wp')."\n\n".$res->getErrorCode().': '.$res->getErrorText()
                            ]);
                            wp_die();
                        }
                    }
                }
            }
        }
        wp_send_json([
            'type' => 'error',
            'message' => __('An error has occurred. Please try later.', 'fivepost-wp')
        ]);
        wp_die();
    }


    public function actionWpAjaxFivepostGetOrderStatus() {
        if(!empty($_POST['order_id'])) {
            if ($wc_order = wc_get_order(wc_sanitize_order_id($_POST['order_id']))) {
                require_once(FIVEPOST_PLUGIN_DIR . 'classes/shipping.class.php');
                $fivepost_shipping_method = new \Fivepost_Shipping_Method();
                if ($fivepost_shipping_method->has_settings() && $fivepost_shipping_method->get_option('api_key')) {
                    if ($order_uid = $wc_order->get_meta('_fivepost_order_id')) {

                        $cntl = new MainController($fivepost_shipping_method->get_option('api_key'), ($fivepost_shipping_method->get_option('api_wtype') == 'TEST'));
                        $ans = $cntl->getOrderStatus($order_uid);

                        if ($ans->isSuccess()) {
                            $ansmes=__('Order data successfully received','fivepost-wp').":\n\n";
                            $res = $ans->getData();
                            $r = array_pop($res); //last status only
                            $ansmes.=__('Order number','fivepost-wp').": ".$r['senderoid']."\n";
                            $ansmes.=__('Order in 5P System','fivepost-wp')." №: ".$r['orderuid']."\n";
                            $ansmes.=__('Last modified date','fivepost-wp').": ".$r['dt']."\n";
                            $ansmes.=__('Status','fivepost-wp').": ".(isset($fivepost_shipping_method::get_main_statuses()[$r['status']])?$fivepost_shipping_method::get_main_statuses()[$r['status']]:$r['status'])."\n";
                            $ansmes.=__('Execution status','fivepost-wp').": ".(isset($fivepost_shipping_method::get_execution_statuses()[$r['execstatus']])?$fivepost_shipping_method::get_execution_statuses()[$r['execstatus']]:$r['execstatus'])."\n";
                            $ansmes.="\n\n";
                            $new_orderstatus = $fivepost_shipping_method->map_statuses($r['status'],$r['execstatus']);
                            //Change Order Status...
                            if ($new_orderstatus) {
                                $wc_order->update_status($new_orderstatus);
                                $wc_order->save();
                            }
                            wp_send_json([
                                'type' => 'success',
                                'message' =>$ansmes
                            ]);
                        } else {
                            wp_send_json([
                                'type' => 'error',
                                'message' => __('An error has occurred. Please try later.', 'fivepost-wp')."\n\n".$ans->getErrorCode().': '.$ans->getErrorText()
                            ]);
                        }
                    }
                }
            }
        }
        wp_send_json([
            'type' => 'error',
            'message' => __('An error has occurred. Please try later.', 'fivepost-wp')
        ]);
        wp_die();
    }

    /**
     * Get GABS for order item - adapted for IPOL Application !!!
     *
     * @param \WC_Order_Item_Product $item
     * @param \Fivepost_Shipping_Method $shipping_method
     * @return array
     */
    private function _getItemDimensions(\WC_Order_Item_Product $item, \Fivepost_Shipping_Method $shipping_method): array {
        $itemWeight = $item->get_product()->get_weight();
        $itemLength = $item->get_product()->get_length();
        $itemWidth = $item->get_product()->get_width();
        $itemHeight = $item->get_product()->get_height();

        if (empty($itemWeight))
            $itemWeight = (float)$shipping_method->get_option('default_item_weight');
        if (empty($itemLength))
            $itemLength = (float)$shipping_method->get_option('default_item_length');
        if (empty($itemWidth))
            $itemWidth = (float)$shipping_method->get_option('default_item_width');
        if (empty($itemHeight))
            $itemHeight = (float)$shipping_method->get_option('default_item_height');

        return [
            'weight' => intval(floatval($itemWeight)*1000),
            'length' => intval(floatval($itemLength)*10),
            'width' => intval(floatval($itemWidth)*10),
            'height' => intval(floatval($itemHeight)*10)
        ];
    }

    /**
     * SendOrder form validation - ADMIN
     *
     * @param array $form
     * @return bool
     */
    private function _validateSendForm(array $form): bool {
        $errors = [];

        if(empty($form['first_name']))
            $errors[] = __('Buyer s name is required', 'fivepost-wp');

        if(empty($form['last_name']))
            $errors[] = __('Buyer s lastname is required', 'fivepost-wp');

        if(empty($form['phone']))
            $errors[] = __('Buyer s phone is required', 'fivepost-wp');
        elseif (!preg_match('/(\+79[0-9]{9})/', $form['phone']))
            $errors[] = __('Invalid phone format. Transmitted data format: +79XXXXXXXXX', 'fivepost-wp');

        $res = filter_var($form['email'], FILTER_VALIDATE_EMAIL);

        if(!empty($form['email']) && !filter_var($form['email'], FILTER_VALIDATE_EMAIL))
            $errors[] = __('You must enter a valid buyer s e-mail', 'fivepost-wp');

        if(empty($form['package_weight']))
            $errors[] = __('Weight cannot be zero', 'fivepost-wp');
        elseif(!is_numeric($form['package_weight']))
            $errors[] = __('Weight must be a number', 'fivepost-wp');

        if(empty($form['package_length']) || empty($form['package_width']) || empty($form['package_height']))
            $errors[] = __('You need to set the dimensions of the parcel', 'fivepost-wp');
        elseif(!is_numeric($form['package_length']) || !is_numeric($form['package_width']) || !is_numeric($form['package_height']))
            $errors[] = __('Dimensions must be specified as a numerical value', 'fivepost-wp');

        if(!empty($errors)) {
            wp_send_json([
                'type' => 'error',
                'message' => implode("\r\n", $errors)
            ]);
            wp_die();
        }

        return true;
    }

    /**
     * Get GOODS items for SendOrder - FOR_DELETE - NOT USED!
     *
     * @param \WC_Order $wc_order
     * @param \Fivepost_Shipping_Method $shipping_method
     *
     * @return ProductValueList
     */
    private function _getProductValueList(\WC_Order $wc_order, \Fivepost_Shipping_Method $shipping_method): ProductValueList
    {
        $product_value_list = new ProductValueList();

        foreach($wc_order->get_items() as $wc_item) {
            /**
             * @var \WC_Order_Item_Product $wc_item
             */
            $product_value = new ProductValue();
            $product_value->setBarcode(null)
                ->setCodeGTD(null)
                ->setCodeTNVED(null)
                ->setCurrency(null)
                ->setOriginCountry(null)
                ->setVendorCode(null)
                ->setName($wc_item->get_product()->get_name())
                ->setPrice(floatval($wc_item->get_product()->get_price()))
                ->setValue($wc_item->get_quantity())
                ->setVat((int)$shipping_method->get_option('shipping_nds'));
            $product_value_list->add($product_value);
        }

        return $product_value_list;
    }

    /**
     * BarCode Generator
     *
     * @param \Fivepost_Shipping_Method $shipping_method
     *
     * @return string
     */
    private function _generateBarcode(\Fivepost_Shipping_Method $shipping_method) {
        $barcode_part = intval(get_option('fivepost-barcode', 0)) + 1;

        if($barcode_part > 9999999999)
            $barcode_part = 0;

        $barcode = sprintf('%010d', $barcode_part);

        update_option('fivepost-barcode', $barcode_part);

        return $shipping_method->get_option('partner_number').$barcode;
    }

    /**
     * Get CargoList for SendOrder - FOR_DELETE - NOT USED!
     *
     * @param array $form
     * @param \WC_Order $wc_order
     * @param \Fivepost_Shipping_Method $shipping_method
     *
     * @return CargoList
     */
    private function _getCargoList($form, \WC_Order $wc_order, \Fivepost_Shipping_Method $shipping_method): CargoList
    {
        $cargo_list = new CargoList();
        $cargo = new Cargo();

        $product_value_list = $this->_getProductValueList($wc_order, $shipping_method);
        $barcode_number = $this->_generateBarcode($shipping_method);

        $barcode_list = new BarcodeList();
        $barcode = new Barcode($barcode_number);
        $barcode_list->add($barcode);
        $cargo_total = 0;
        $cargo_height = floatval(strtr($form['package_height'], [',' => '.'])) * 10;
        $cargo_length = floatval(strtr($form['package_length'], [',' => '.'])) * 10;
        $cargo_width = floatval(strtr($form['package_width'], [',' => '.'])) * 10;
        $cargo_weight = floatval(strtr($form['package_weight'], [',' => '.'])) * 1000000;

        foreach($product_value_list->getFields() as $product)
            $cargo_total += $product['price'] * $product['value'];

        $cargo->setProductValues($product_value_list)
            ->setBarcodes($barcode_list)
            ->setSenderCargoId($barcode_number)
            ->setPrice($cargo_total)
            ->setVat((int)$shipping_method->get_option('shipping_nds'))
            ->setCurrency('RUB')
            ->setHeight($cargo_height)
            ->setLength($cargo_length)
            ->setWidth($cargo_width)
            ->setWeight($cargo_weight);
        $cargo_list->add($cargo);

        return $cargo_list;
    }

    /**
     * Getting a list of products when sending an order - FOR_DELETE - NOT USED!
     *
     * @param array $form
     * @param \WC_Order $wc_order
     *
     * @return Cost
     */
    private function _getCost(array $form, \WC_Order $wc_order): Cost
    {
        $delivery_cost = 0;
        $price = 0;
        $payment_value = 0;
        $payment_type = 'PREPAYMENT';

        foreach($wc_order->get_items() as $wc_item) {
            /**
             * @var \WC_Order_Item_Product $wc_item
             */
            $price += floatval($wc_item->get_subtotal());
        }

        foreach($wc_order->get_items('shipping') as $shipping_item) {
            /**
             * @var \WC_Order_Item_Shipping $shipping_item
             */
            if($shipping_item->get_method_id() == \Fivepost_Shipping_Method::SHIPPING_METHOD_ID) {
                $delivery_cost = floatval($shipping_item->get_total());
                break;
            }
        }

        if(empty($form['full_paid'])) {
            $payment_value = floatval($wc_order->get_total());
            $payment_type = 'CASHLESS';
        }

        $cost = new Cost();
        $cost->setDeliveryCost($delivery_cost)
            ->setDeliveryCostCurrency('RUB')
            ->setPaymentCurrency('RUB')
            ->setPaymentType($payment_type)
            ->setPaymentValue($payment_value)
            ->setPrice($price)
            ->setPriceCurrency('RUB');

        return $cost;
    }

    /**
     * Getting information about an order when creating it - FOR_DELETE - NOT USED!
     *
     * @param array $form
     * @param \WC_Order $wc_order
     * @param \stdClass $warehouse
     * @param CargoList $cargo_list
     * @param Cost $cost
     *
     * @return CreateOrder
     */
    private function _getCreateOrder(array $form, \WC_Order $wc_order, \stdClass $warehouse, CargoList $cargo_list, Cost $cost): CreateOrder
    {
        $create_order = new CreateOrder();
        $partner_order_list = new PartnerOrderList();
        $partner_order = new PartnerOrder();

        $partner_order->setClientEmail(!empty($form['email']) ? : null)
            ->setPlannedReceiveDate(null)
            ->setSenderCreateDate(null)
            ->setShipmentDate(null)
            ->setSenderOrderId('WP_'.$wc_order->get_id())//.'_'.time())
            ->setBrandName(get_bloginfo('name'))
            ->setClientOrderId('WP_'.$wc_order->get_order_number())
            ->setClientName($form['last_name'].' '.$form['first_name'])
            ->setClientPhone($form['phone'])
            ->setReceiverLocation($wc_order->get_meta('_fivepost_point_id'))
            ->setSenderLocation($warehouse->partner_location_id)
            ->setUndeliverableOption('RETURN')
            ->setCargoes($cargo_list)
            ->setCost($cost);
        $partner_order_list->add($partner_order);
        $create_order->setPartnerOrders($partner_order_list);

        return $create_order;
    }
}