<?php
namespace Ipol\Fivepost\WordPress\Controller;

use Ipol\Fivepost\Core\Order\OrderCollection;
use Ipol\Fivepost\Core\Order\Order;
use Ipol\Fivepost\WordPress\Entity\BasicResponse;

class MainController extends abstractController {
    /**
     * @var \Ipol\Fivepost\Core\Order\Order
     */
    protected $order;

    public function __construct($apikey, $testmode=false)
    {
        parent::__construct($apikey, $testmode);
        //$this->order = $order;
    }

    /**
     * @return BasicResponse
     */
    public function Send() {
        $orderCollection = new OrderCollection();
        $orderCollection->add($this->order);

        $result = new BasicResponse();

        $ans = $this->application->ordersMake($orderCollection);

        //$ans = $this->application->sendOrders($orderCollection);
        if ($ans->isSuccess()) {
            $result->setSuccess(true)->setData($ans);
        } else {
            $errors = [];
            if ($this->application->getErrorCollection()) {
                $this->application->getErrorCollection()->reset();
                while ($error = $this->application->getErrorCollection()->getNext()) {
                    $errors[] = $error->getMessage();
                }
            } else {
                $errors[] = 'Error while sending order, but no error messages get from application.';
            }
            $result->setSuccess(false)->setErrorText(implode("\n", $errors));
        }
        return $result;
    }

    /**
     * @param String $orderuid
     * @return basicresponse
     */
    public function printLabels(String $orderuid) {
        $ans = $this->application->getOrderLabels([$orderuid]);
        $res = new BasicResponse();
        if ($ans->isSuccess()) {
            $resultsCollection = $ans->getResponse()->getOrderLabelsResults();
            $resultsCollection->reset();
            while ($orderLabel = $resultsCollection->getNext()) {
                //$oId = $orderLabel->getSenderOrderId() ?: $orderLabel->getOrderId();
                if ($orderLabel->isSuccess()) {
                    $res->setSuccess(true)->setData($orderLabel->getFileName());
                    file_put_contents(FIVEPOST_PLUGIN_DIR.'files/'.$orderLabel->getFileName(),$orderLabel->getFileContent());
                } else {
                    $res->setSuccess(false)->setErrorCode(1)->setErrorText($orderLabel->getReason());
                }
            }
        } else {
            if ($this->application->getErrorCollection()) {
                $this->application->getErrorCollection()->reset();
                $res->setSuccess(false)->setErrorCode(2)->setErrorText($ans->getResponse()->getError());
            } else $res->setSuccess(false)->setErrorCode(3)->setErrorText('Error while getting order labels from API, but no error messages get from application.');
        }
        return $res;
    }

    /**
     * @param String $orderuid
     * @return BasicResponse
     */
    public function getOrderStatus(String $orderuid) {
        $ans = $this->application->getOrderStatus([$orderuid]);
        $res = new BasicResponse();
        if ($ans->isSuccess()) {
            $resCollection = $ans->getResponse()->getOrderStatuses();
            $resCollection->reset();
            $rs=[];
            while ($st = $resCollection->getNext()) {
                $d = new \DateTime($st->getChangeDate());
              $rs[]=[
                  'status'=>$st->getStatus(),
                  'execstatus'=>$st->getExecutionStatus(),
                  'dt'=>$d->format('m.d.Y H:i:s'), //2022-03-22T16:55:36.465259+03:00
                  'orderuid'=>$st->getOrderId(),
                  'senderoid'=>$st->getSenderOrderId()
              ];
            }
            $res->setSuccess(true)->setData($rs);
        } else {
            $res->setSuccess(false)->setErrorCode($ans->getError()->getCode())->setErrorText($ans->getResponse()->getError());
        }
        return $res;
    }

    /**
     * @return \Ipol\Fivepost\Core\Order\Order
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * @param \Ipol\Fivepost\Core\Order\Order $order
     * @return $this
     */
    public function setOrder(Order $order) {
        $this->order = $order;
        return $this;
    }

}