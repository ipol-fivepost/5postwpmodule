<?php

namespace Ipol\Fivepost\WordPress\Controller;

use Ipol\Fivepost\Fivepost\FivepostApplication;

abstract class abstractController {
    /**
     * @var Options module options entity
     */
    protected $options;

    /**
     * @var Cache entity for cache
     */
    protected $cache;

    /**
     * @var Logger entity for logs
     */
    protected $logger;

    /**
     * @var FivepostApplication entity for API calls
     */
    protected $application;

    public function __construct($apikey,$testmode=false) {
        $this->cache=null;
        $this->logger=null;

        //$this->options = new Options();

        $this->application = new FivepostApplication(
            $apikey,
            $testmode,
            10,
            null,
            $this->cache
        );
        //$this->application->setOption($this->options);
    }

}
