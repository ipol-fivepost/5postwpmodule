<?php

spl_autoload_register(function($className) {
    if (strpos($className, FIVEPOST_LIB_NAMESPACE) === 0) {
        $classPath = implode(DIRECTORY_SEPARATOR, explode('\\', substr($className,14)));
        $filePath = __DIR__.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.$classPath.'.php';

        if (is_readable($filePath) && file_exists($filePath))
            require_once $filePath;
    } elseif (strpos($className, FIVEPOST_NAMESPACE) === 0) {
        $classFile = strtr(substr(mb_strtolower($className),19), ['_' => '-']).'.class';
        $filePath = __DIR__.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.$classFile.'.php';

        if (is_readable($filePath) && file_exists($filePath))
            require_once $filePath;
    }
});